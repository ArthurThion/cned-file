﻿/**
 * tp 4 : contacts
 * author : Emds
 * date : 03/08/2017
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace tp4
{
    public partial class frmContact : Form
    {
        // propriétés
        List<Contact> lesContacts = new List<Contact>(); // liste des contacts
        string fichier = "ficcontact"; // nom du fichier de sérialisation


        public frmContact()
        {
            InitializeComponent();
        }

        /**
         * Trouve un contact avec son tel dans la liste
         */
        private Contact trouveContactParTel(string tel)
        {
            Contact leContact = null;
            foreach (Contact unContact in lesContacts)
            {
                if (unContact.getTel().Equals(tel))
                {
                    leContact = unContact;
                }
            }
            return leContact;
        }

        /**
         * Trouve l'indice de la ligne dans le listbox par rapport au tel
         */
        private int trouveLigneParTel(string tel)
        {
            int indice = 0;
            // recherche de la ligne comportant le téléphone
            while (indice < lstContact.Items.Count - 1
                && !extraireTel(lstContact.Items[indice].ToString()).Equals(tel)) { 
                {
                indice++;
                }
            }
            // si la ligne est trouvée, son indice est retourné
            if (extraireTel(lstContact.Items[indice].ToString()).Equals(tel))
            {
                return indice;
            }
            else
            {
                return -1;
            }
        }

        /**
         * Trouve un contact par rapport à une ligne dans la listbox
         */
        private Contact trouveContact(String ligne)
        {
            // récupère le téléphone de cette ligne
            string tel = extraireTel(ligne);
            // retourne le contact correspondant
            return trouveContactParTel(tel);
        }

        /**
         * Préparation à la saisie d'un ajout ou fin de l'ajout
         * active ou désactive les composants
         */
        private void gestionComposantsAjout(bool actif)
        {
            // Active ou non les composants pour l'ajout
            txtNom.Enabled = actif;
            txtPrenom.Enabled = actif;
            txtTel.Enabled = actif;
            imgPhoto.Enabled = actif;
            btnAjouter.Enabled = actif;
            btnAnnuler.Enabled = actif;
            grbRechercher.Enabled = !actif;
            lstContact.Enabled = !actif;
            btnModif.Enabled = !actif;
            btnSuppr.Enabled = !actif;
            // si début d'ajout
            if (actif)
            {
                // affiche ou non le prénom
                lblPrenom.Visible = rdbParticulier.Checked;
                txtPrenom.Visible = rdbParticulier.Checked;
                // gestion de la photo par défaut
                affichePhotoStandard();
                // affiche le message pour indiquer comment ajouter une photo
                lblChoixPhoto.Visible = true;
                // positionnement sur le nom
                txtNom.Focus();
            }
            else
            {
                // fin d'ajout
                // vide l'image
                videPhoto();
                // affiche le message pour indiquer comment ajouter une photo
                lblChoixPhoto.Visible = false;
                // vide les zones de saisie
                txtNom.Text = "";
                txtPrenom.Text = "";
                txtTel.Text = "";
                // décoche les 2 boutons radio
                rdbParticulier.Checked = false;
                rdbProfessionnel.Checked = false;
                // place le focus sur la liste
                lstContact.Focus();
            }
            // désactive la ligne sélectionnée dans la liste
            lstContact.SelectedIndex = -1;
        }

        /**
         * Mise à jour de la listbox avec tous les contacts
         * et positionement sur le nouvel indice si spécifié
         */
        private void majListBox(int indice)
        {
            // vide la liste avant de la remplir à nouveau
            lstContact.Items.Clear();
            // remplissage de la listbox à partir de la collection
            foreach(Contact unContact in lesContacts) {
                lstContact.Items.Add(unContact.infosContact());
            }
            // positionnement sur le nouvel indice (si possible)
            try
            {
                lstContact.SelectedIndex = indice;
            }catch(Exception ex)
            {
                lstContact.SelectedIndex = -1;
                videPhoto();
            }
            // place le focus sur la liste
            lstContact.Focus();
            // sauve la liste dans le fichier
            Serialise.Sauve(fichier, lesContacts);
        }

        /**
         * Mise à jour de la listbox en fonction de la recherche sur le téléphone
         */
        private void majListBoxRecherche()
        {
            lstContact.Items.Clear();
            foreach (Contact unContact in lesContacts)
            {
                if (unContact.getTel().IndexOf(txtRecherche.Text) != -1)
                {
                    lstContact.Items.Add(unContact.infosContact());
                }
            }
            // affiche une image vide
            videPhoto();
        }

        /**
         * Vide l'affichage de la photo
         */
        private void videPhoto()
        {
            imgPhoto.Image = Properties.Resources.vide;
        }

        /**
         * Affichage de la photo standard
         */
        private void affichePhotoStandard()
        {
            if (rdbParticulier.Checked)
            {
                imgPhoto.Image = Properties.Resources.particulier;
            }
            else
            {
                imgPhoto.Image = Properties.Resources.professionnel;
            }
        }

        /**
         * Suppression d'un contact par rapport à son indice de ligne
         */
        private void supprContact(int indice)
        {
            // trouve le contact correspondant à l'indice de ligne
            Contact unContact = trouveContact(lstContact.Items[indice].ToString());
            // suppression du contact
            lesContacts.Remove(unContact);
            // mets à jour la listbox
            majListBox(indice);
        }

        /**
         * Permet d'extraire le tel d'une ligne de contact 
         * le tel étant mis entre parenthèses
         */
        private string extraireTel(string unContact)
        {
            int position = unContact.IndexOf('(');
            string tel = unContact.Substring(position + 1);
            return tel.Substring(0, tel.Length - 1);
        }

        /**
         * Remplit les zones d'ajout avec les informations du contact
         */
        private void remplirContact(Contact unContact)
        {
            if(unContact is Particulier)
            {
                rdbParticulier.Checked = true;
                txtPrenom.Text = ((Particulier)unContact).getPrenom();
            }
            else
            {
                rdbProfessionnel.Checked = true;
            }
            txtNom.Text = unContact.getNom();
            txtTel.Text = unContact.getTel();
            imgPhoto.Image = unContact.getPhoto();
        }

        /**
         * Evénement clic sur bouton annule recherche
         * remplit la liste de tous les contacts
         */
        private void btnAnnule_Click(object sender, EventArgs e)
        {
            txtRecherche.Text = "";
        }

        /**
         * Evénement touche appuyée dans zone de recherche
         * Recherche automatique des contacts contenant ce tel (en partie ou totalité)
         */
        private void txtRecherche_TextChanged(object sender, EventArgs e)
        {
            // si la rechreche est vide, la liste est totalement remplie
            if (txtRecherche.Text.Equals(""))
            {
                majListBox(0);
            }
            else
            {
                // remplissage de la liste par rapport à la recherche
                majListBoxRecherche();
            }
        }

        /**
         * Evénement clic sur le bouton suppr
         * supprime le contact sélectionné
         */
        private void btnSuppr_Click(object sender, EventArgs e)
        {
            //  contrôler qu'une ligne est bien sélectionnée
            if(lstContact.SelectedIndex != -1)
            {
                // récupère l'indice de la ligne à supprimer
                int indice = lstContact.SelectedIndex;
                // suppression du contact
                supprContact(indice);
                // vide la zone de recherche
                txtRecherche.Text = "";
            }
        }

        /**
         * Evénement sélection d'un contact dans la liste
         * chargement de la photo
         */
        private void lstContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            // si une ligne est sélectionnée
            if(lstContact.SelectedIndex != -1)
            {
                // récupération du contact
                Contact leContact = trouveContact(lstContact.SelectedItem.ToString());
                // affichage de l'image
                imgPhoto.Image = leContact.getPhoto();
            }
        }

        /**
         * Evénement clic sur bouton Ajouter
         * Ajout du contact dans la liste
         */
        private void btnAjouter_Click(object sender, EventArgs e)
        {
             // vérifier que le nom et le tel ne sont pas vides 
            // et le prenom dans le cas d'un particulier)
            if (!txtNom.Text.Equals("") && !txtTel.Text.Equals("")
                && ((rdbParticulier.Checked && !txtPrenom.Text.Equals("")) || rdbProfessionnel.Checked))
            {
                // si un contact n'existe pas déjà avec ce tel, l'ajout peut se faire
                if (trouveContactParTel(txtTel.Text) == null)
                {
                    // récupération du téléphone
                    string tel = txtTel.Text;
                    // création du contact
                    Contact nouveauContact;
                    if (rdbParticulier.Checked)
                    {
                        nouveauContact = new Particulier(txtNom.Text, txtPrenom.Text, tel, imgPhoto.Image);
                    }
                    else
                    {
                        nouveauContact = new Professionnel(txtNom.Text, tel, imgPhoto.Image);
                    }
                    // ajout du contact dans la collection
                    lesContacts.Add(nouveauContact);
                    // mise à jour de la liste box
                    majListBox(-1);
                    // désactivation de la zone d'ajout
                    gestionComposantsAjout(false);
                    lstContact.SelectedIndex = trouveLigneParTel(tel);
                }
                else
                {
                    MessageBox.Show("Ce téléphone existe déjà");
                }
            }
            else
            {
                MessageBox.Show("Toutes les zones sont obligatoires");
            }
        }

        /**
         * Evénement clic sur le bouton de modification d'un contact
         * Supprime le contact et transfère ces informations dans la zone d'ajout
         */
        private void btnModif_Click(object sender, EventArgs e)
        {
            // contrôle si un contact est sélectionné
            if(lstContact.SelectedIndex != -1)
            {
                // récupère l'indice de ligne
                int indice = lstContact.SelectedIndex;
                // récupère le contact
                Contact leContact = trouveContact(lstContact.Items[indice].ToString());
                // supprime le contact
                supprContact(indice);
                // vide la zone de recherche
                txtRecherche.Text = "";
                // remplit les zones d'ajout avec les informations du contact
                remplirContact(leContact);
            }
        }

        /**
         * Evénement écriture d'une ligne de la liste
         * gère les couleurs
         */
        private void lstContact_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index != -1)
            {
                string ligne = lstContact.Items[e.Index].ToString();
                // Colore le fond de la ligne
                e.DrawBackground();
                // Définit la couleur du texte (particulier ou professionnel)
                Color uneCouleur;
                if (trouveContact(ligne) is Particulier)
                {
                    uneCouleur = rdbParticulier.ForeColor;
                }
                else
                {
                    uneCouleur = rdbProfessionnel.ForeColor;
                }
                // fixe la couleur
                Brush myBrush = new SolidBrush(uneCouleur);
                // écrit la ligne en utilisant la couleur définie
                e.Graphics.DrawString(ligne, e.Font, myBrush, e.Bounds, StringFormat.GenericDefault);
                // Si la listbox a le focus, dessine le rectangle qui l'entoure
                e.DrawFocusRectangle();
            }
        }

        /**
         * Evénement sélection du bouton radio Particulier
         * Faire apparaitre ou disparaitre le prénom (ne concerne que les particuliers)
         */
        private void rdbParticulier_CheckedChanged(object sender, EventArgs e)
        {
            // si c'est la première sélection
            if(rdbParticulier.Checked == true && rdbProfessionnel.Checked == false)
            {
                gestionComposantsAjout(true);
            }         
        }

        /**
         * Evénement sélection du bouton radio professionnel
         */
        private void rdbProfessionnel_CheckedChanged(object sender, EventArgs e)
        {
            // si c'est la première sélection
            if (rdbParticulier.Checked == false && rdbProfessionnel.Checked == true)
            {
                gestionComposantsAjout(true);
            }
        }

        /**
         * Evénement chargement de la fenêtre
         * préparation des composants
         */
        private void frmContact_Load(object sender, EventArgs e)
        {
            // préparation des composants graphiques
            gestionComposantsAjout(false);
            // récupération de la sauvegarde des contacts, si elle existe
            Object recupContacts = Serialise.Recup(fichier);
            if (recupContacts != null)
            {
                lesContacts = (List<Contact>)recupContacts;
                // remplissage de la listbox avec les contacts récupérés
                majListBox(0);
            }
        }

        /**
         * Evénement clic sur le bouton annuler de l'ajout
         */
        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            gestionComposantsAjout(false);
            // mise à jour de la liste box (pour la raffraichir)
            majListBox(0);
        }

        /**
         * Evénement clic sur la photo
         * possibilité de sélectionner une photo
         */
        private void imgPhoto_Click(object sender, EventArgs e)
        {
            // boite de dialogue pour sélectionner un fichier
            OpenFileDialog rechercheFichier;
            rechercheFichier = new System.Windows.Forms.OpenFileDialog();
            DialogResult choix = rechercheFichier.ShowDialog();
            // si un fichier est sélectionné
            if (choix == DialogResult.OK)
            {
                // récupération du fichier
                string nomFichier = rechercheFichier.FileName;
                // tente d'afficher l'image
                try
                {
                    imgPhoto.Image = Image.FromFile(nomFichier);
                }catch(Exception ex)
                {
                    // erreur le fichier n'est pas une image
                    MessageBox.Show("Le fichier n'est pas une image");
                    // affichage de l'image par défaut
                    affichePhotoStandard();
                }
            }
        }

        /**
         * Evénement clic sur le label pour sélectionner une photo
         * mêmes traitements que le clic sur la photo
         */
        private void lblChoixPhoto_Click(object sender, EventArgs e)
        {
            imgPhoto_Click(null, null);
        }
    }

}
