﻿namespace tp4
{
    partial class frmContact
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbAjout = new System.Windows.Forms.GroupBox();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.rdbProfessionnel = new System.Windows.Forms.RadioButton();
            this.rdbParticulier = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lstContact = new System.Windows.Forms.ListBox();
            this.grbRechercher = new System.Windows.Forms.GroupBox();
            this.txtRecherche = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.imgPhoto = new System.Windows.Forms.PictureBox();
            this.btnAnnuleRecherche = new System.Windows.Forms.Button();
            this.btnModif = new System.Windows.Forms.Button();
            this.btnSuppr = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.lblChoixPhoto = new System.Windows.Forms.Label();
            this.grbAjout.SuspendLayout();
            this.grbRechercher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // grbAjout
            // 
            this.grbAjout.Controls.Add(this.btnAnnuler);
            this.grbAjout.Controls.Add(this.btnAjouter);
            this.grbAjout.Controls.Add(this.txtTel);
            this.grbAjout.Controls.Add(this.label3);
            this.grbAjout.Controls.Add(this.txtPrenom);
            this.grbAjout.Controls.Add(this.lblPrenom);
            this.grbAjout.Controls.Add(this.txtNom);
            this.grbAjout.Controls.Add(this.rdbProfessionnel);
            this.grbAjout.Controls.Add(this.rdbParticulier);
            this.grbAjout.Controls.Add(this.label1);
            this.grbAjout.Location = new System.Drawing.Point(12, 218);
            this.grbAjout.Name = "grbAjout";
            this.grbAjout.Size = new System.Drawing.Size(229, 142);
            this.grbAjout.TabIndex = 0;
            this.grbAjout.TabStop = false;
            this.grbAjout.Text = "ajout contact";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(63, 111);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(106, 20);
            this.txtTel.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "tel :";
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(63, 79);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(106, 20);
            this.txtPrenom.TabIndex = 5;
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(6, 82);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(51, 13);
            this.lblPrenom.TabIndex = 4;
            this.lblPrenom.Text = "prénom : ";
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(63, 47);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(106, 20);
            this.txtNom.TabIndex = 3;
            // 
            // rdbProfessionnel
            // 
            this.rdbProfessionnel.AutoSize = true;
            this.rdbProfessionnel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbProfessionnel.ForeColor = System.Drawing.Color.Green;
            this.rdbProfessionnel.Location = new System.Drawing.Point(82, 19);
            this.rdbProfessionnel.Name = "rdbProfessionnel";
            this.rdbProfessionnel.Size = new System.Drawing.Size(87, 17);
            this.rdbProfessionnel.TabIndex = 2;
            this.rdbProfessionnel.Text = "professionnel";
            this.rdbProfessionnel.UseVisualStyleBackColor = true;
            this.rdbProfessionnel.CheckedChanged += new System.EventHandler(this.rdbProfessionnel_CheckedChanged);
            // 
            // rdbParticulier
            // 
            this.rdbParticulier.AutoSize = true;
            this.rdbParticulier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbParticulier.ForeColor = System.Drawing.Color.Purple;
            this.rdbParticulier.Location = new System.Drawing.Point(6, 19);
            this.rdbParticulier.Name = "rdbParticulier";
            this.rdbParticulier.Size = new System.Drawing.Size(70, 17);
            this.rdbParticulier.TabIndex = 1;
            this.rdbParticulier.Text = "particulier";
            this.rdbParticulier.UseVisualStyleBackColor = true;
            this.rdbParticulier.CheckedChanged += new System.EventHandler(this.rdbParticulier_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "nom :";
            // 
            // lstContact
            // 
            this.lstContact.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lstContact.FormattingEnabled = true;
            this.lstContact.HorizontalScrollbar = true;
            this.lstContact.Location = new System.Drawing.Point(247, 12);
            this.lstContact.Name = "lstContact";
            this.lstContact.Size = new System.Drawing.Size(311, 420);
            this.lstContact.Sorted = true;
            this.lstContact.TabIndex = 3;
            this.lstContact.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstContact_DrawItem);
            this.lstContact.SelectedIndexChanged += new System.EventHandler(this.lstContact_SelectedIndexChanged);
            // 
            // grbRechercher
            // 
            this.grbRechercher.Controls.Add(this.btnAnnuleRecherche);
            this.grbRechercher.Controls.Add(this.txtRecherche);
            this.grbRechercher.Controls.Add(this.label4);
            this.grbRechercher.Location = new System.Drawing.Point(12, 366);
            this.grbRechercher.Name = "grbRechercher";
            this.grbRechercher.Size = new System.Drawing.Size(229, 65);
            this.grbRechercher.TabIndex = 5;
            this.grbRechercher.TabStop = false;
            this.grbRechercher.Text = "recherche";
            // 
            // txtRecherche
            // 
            this.txtRecherche.Location = new System.Drawing.Point(63, 13);
            this.txtRecherche.Name = "txtRecherche";
            this.txtRecherche.Size = new System.Drawing.Size(106, 20);
            this.txtRecherche.TabIndex = 1;
            this.txtRecherche.TextChanged += new System.EventHandler(this.txtRecherche_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "tel :";
            // 
            // imgPhoto
            // 
            this.imgPhoto.Enabled = false;
            this.imgPhoto.Image = global::tp4.Properties.Resources.vide;
            this.imgPhoto.Location = new System.Drawing.Point(12, 12);
            this.imgPhoto.Name = "imgPhoto";
            this.imgPhoto.Size = new System.Drawing.Size(170, 200);
            this.imgPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPhoto.TabIndex = 6;
            this.imgPhoto.TabStop = false;
            this.imgPhoto.Click += new System.EventHandler(this.imgPhoto_Click);
            // 
            // btnAnnuleRecherche
            // 
            this.btnAnnuleRecherche.Image = global::tp4.Properties.Resources.playagain;
            this.btnAnnuleRecherche.Location = new System.Drawing.Point(175, 13);
            this.btnAnnuleRecherche.Name = "btnAnnuleRecherche";
            this.btnAnnuleRecherche.Size = new System.Drawing.Size(45, 45);
            this.btnAnnuleRecherche.TabIndex = 2;
            this.btnAnnuleRecherche.UseVisualStyleBackColor = true;
            this.btnAnnuleRecherche.Click += new System.EventHandler(this.btnAnnule_Click);
            // 
            // btnModif
            // 
            this.btnModif.Image = global::tp4.Properties.Resources.modifier;
            this.btnModif.Location = new System.Drawing.Point(196, 12);
            this.btnModif.Name = "btnModif";
            this.btnModif.Size = new System.Drawing.Size(45, 45);
            this.btnModif.TabIndex = 4;
            this.btnModif.UseVisualStyleBackColor = true;
            this.btnModif.Click += new System.EventHandler(this.btnModif_Click);
            // 
            // btnSuppr
            // 
            this.btnSuppr.Image = global::tp4.Properties.Resources.supprimer;
            this.btnSuppr.Location = new System.Drawing.Point(196, 63);
            this.btnSuppr.Name = "btnSuppr";
            this.btnSuppr.Size = new System.Drawing.Size(45, 45);
            this.btnSuppr.TabIndex = 3;
            this.btnSuppr.UseVisualStyleBackColor = true;
            this.btnSuppr.Click += new System.EventHandler(this.btnSuppr_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Image = global::tp4.Properties.Resources.annuler;
            this.btnAnnuler.Location = new System.Drawing.Point(175, 86);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(45, 45);
            this.btnAnnuler.TabIndex = 9;
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnAjouter
            // 
            this.btnAjouter.Image = global::tp4.Properties.Resources.ajouter;
            this.btnAjouter.Location = new System.Drawing.Point(175, 34);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(45, 45);
            this.btnAjouter.TabIndex = 8;
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // lblChoixPhoto
            // 
            this.lblChoixPhoto.BackColor = System.Drawing.Color.Transparent;
            this.lblChoixPhoto.Enabled = false;
            this.lblChoixPhoto.Location = new System.Drawing.Point(31, 167);
            this.lblChoixPhoto.Name = "lblChoixPhoto";
            this.lblChoixPhoto.Size = new System.Drawing.Size(125, 32);
            this.lblChoixPhoto.TabIndex = 7;
            this.lblChoixPhoto.Text = "Cliquer pour sélectionner une photo";
            this.lblChoixPhoto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChoixPhoto.Visible = false;
            this.lblChoixPhoto.Click += new System.EventHandler(this.lblChoixPhoto_Click);
            // 
            // frmContact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 439);
            this.Controls.Add(this.lblChoixPhoto);
            this.Controls.Add(this.imgPhoto);
            this.Controls.Add(this.lstContact);
            this.Controls.Add(this.grbRechercher);
            this.Controls.Add(this.btnModif);
            this.Controls.Add(this.btnSuppr);
            this.Controls.Add(this.grbAjout);
            this.Name = "frmContact";
            this.Text = "Contacts";
            this.Load += new System.EventHandler(this.frmContact_Load);
            this.grbAjout.ResumeLayout(false);
            this.grbAjout.PerformLayout();
            this.grbRechercher.ResumeLayout(false);
            this.grbRechercher.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbAjout;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.RadioButton rdbProfessionnel;
        private System.Windows.Forms.RadioButton rdbParticulier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.ListBox lstContact;
        private System.Windows.Forms.Button btnSuppr;
        private System.Windows.Forms.Button btnModif;
        private System.Windows.Forms.GroupBox grbRechercher;
        private System.Windows.Forms.TextBox txtRecherche;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAnnuleRecherche;
        private System.Windows.Forms.PictureBox imgPhoto;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label lblChoixPhoto;
    }
}

