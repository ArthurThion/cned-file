﻿/**
 * tp 3 : pendu
 * author : Emds
 * date : 02/08/2017
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace tp3
{
    public partial class frmPendu : Form
    {
        // propriétés
        private char[] tAlpha = new char[26]; // tableau des lettres
        private int sizeButton = 30; // taille du côté d'un bouton de lettre
        private int posXButton = 6; // position du 1er bouton à partir de la gauche 
        private int posYButton = 18; // position du 1er bouton à partir du haut
        private int nbLettreParLigne = 9; // détermine le nombre de boutons de lettres par ligne
        private string mot; // mot à chercher
        private int etapePendu; // étapes d'affichage du pendu
        private int maxPendu = 10; // maximum d'étapes du pendu

        /**
         * Permet de créer les boutons de l'alphabet
         */
        private void creeBoutons()
        {
            // remplissage du tableau de lettres
            for(int k = 0; k < tAlpha.Length; k++)
            {
                tAlpha[k] = (char) ('A' + k);
            }

            // création et positionnement des boutons
            int line = -1, col = 0;
            for(int k=0; k < tAlpha.Length; k++)
            {
                // création du bouton
                Button btn = new Button();
                // ajout du bouton dans le groupe de boutons pour l'affichage
                grpBoutons.Controls.Add(btn);
                // Ajoute la lettre correspondante au bouton
                btn.Text = tAlpha[k].ToString();
                // fixe la taille du bouton
                btn.Size = new Size(sizeButton, sizeButton);
                // ajout d'une écoute sur le clic du bouton
                btn.Click += new System.EventHandler(btnAlpha_Click);
                // changement de ligne au bout d'un certain nombre de boutons affichés
                col++;
                if (k%nbLettreParLigne == 0)
                {
                    line++;
                    col = 0;
                }
                // positionne le bouton dans le groupe
                btn.Location = new Point(posXButton+sizeButton*col, posYButton+sizeButton*line);
            }
        }

        /**
         * Afichage d'une étape du pendu
         */
        private bool affichePendu()
        {
            etapePendu++;
            afficheImage(etapePendu);
            return (etapePendu == maxPendu);
        }

        /**
         * Recherche la lettre dans le mot et remplace le tiret par la lettre
         * retourne vrai si la lettre est trouvée au moins une fois
         */
        private bool rechercheLettre(char lettre)
        {
            int position = -1; // position de la lettre dans le mot
            bool trouve = false; // pour savoir si la lettre a été trouvée
            // boucle sur la recherche de la lettre (qui peut être présente plusieurs fois)
            do
            {
                // récupère la position de la lettre (ou -1)
                position = mot.IndexOf(lettre, position+1);
                if(position != -1)
                {
                    trouve = true;
                    // remplace le tiret par la lettre dans la zone de texte
                    txtMot.Text = txtMot.Text.Remove(position, 1);
                    txtMot.Text = txtMot.Text.Insert(position, lettre.ToString());
                }
            } while (position != -1);
            return trouve;
        }

        /**
         * Affiche une image d'une numéro précis
         */
        private void afficheImage(int num)
        {
            imgPendu.ImageLocation = Application.StartupPath+"/../../Resources/pendu" + num + ".png";
        }

        /**
         * Début d'une partie
         */
        private void debutJeu()
        {
            // remet à 0 les étapes du pendu
            etapePendu = 0;
            // supprime le message
            lblResultat.Text = "";
            // désactive les boutons
            activeBoutons(false);
            // vide et active la zone de texte
            txtMot.Text = "";
            txtMot.Enabled = true;
            txtMot.Focus();
            // efface l'image
            afficheImage(0);
        }

        /**
         * Active ou désactive les boutons des lettres
         */
        private void activeBoutons(bool actif)
        {
            // active ou désactive les boutons
            for(int k=0; k < tAlpha.Length; k++)
            {
                grpBoutons.Controls[k].Enabled = actif;
            }
            // si les boutons sont activés, focus sur le premier
            if (actif)
            {
                grpBoutons.Controls[0].Focus();
            }
        }

        /**
         * Le joueur a perdu
         */
        private void perdu()
        {
            // affichage du message
            lblResultat.Text = "PERDU";
            // désactive les boutons des lettres
            activeBoutons(false);
            // affiche le mot correct
            txtMot.Text = mot;
            // se positionne sur le bouton pour recommencer
            btnRejouer.Focus();
        }

        /**
         * le joueur a gagné
         */
        private void gagne()
        {
            // affichage du message
            lblResultat.Text = "GAGNE";
            // désactive les boutons des lettres
            activeBoutons(false);
            // se positionne sur le bouton pour recommencer
            btnRejouer.Focus();
        }

        /**
         * Contrôle si un mot est bien constitué uniquement de lettres
         */
        private bool motCorrect(string unMot)
        {
            unMot = unMot.ToUpper();
            bool correct = true;
            for(int k = 0; k < unMot.Length; k++)
            {
                if(char.Parse(unMot.Substring(k, 1))<'A' || char.Parse(unMot.Substring(k, 1)) > 'Z')
                {
                    correct = false;
                }
            }
            return correct;
        }

        /**
         * Evénement clic sur un des boutons de l'alphabet
         */
        private void btnAlpha_Click(object sender, EventArgs e)
        {
            // désactive le bouton
            ((Button)sender).Enabled = false;
            // recherche la lettre
            char lettre = char.Parse(((Button)sender).Text);
            if (!rechercheLettre(lettre))
            {
                // lettre non trouvée : affichage du pendu
                if (affichePendu())
                {
                    // si totalement pendu, perdu et fin du jeu
                    perdu();
                }
            }
            else
            {
                // il n'y a plus de lettre à trouver
                if (txtMot.Text.IndexOf('-') == -1)
                {
                    // s'il n'y a plus de tiret (toutes les lettres trouvées) c'est gagné
                    gagne();
                }
            }
        }

        /**
         * Evénement chargement de la fenêtre
         * création des boutons des lettres de l'alphabet
         * et démarrage du jeu
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            creeBoutons();
            debutJeu();
        }

        /**
         * Evénement touche enfoncée dans la zone du mot
         * si validation, enregistrement du mot et début du jeu
         */
        private void txtMot_KeyPress(object sender, KeyPressEventArgs e)
        {
            // validation donc fin de la saisie du mot
            if(e.KeyChar == (char)Keys.Enter)
            {
                // vérifie qu'un mot a été tapé et qu'il est correct (uniquement lettres)
                if (!txtMot.Text.Equals("") && motCorrect(txtMot.Text))
                {
                    // enregistrement du mot en majuscule
                    mot = txtMot.Text.ToUpper();
                    // désactive la zone et la remplit de tirets
                    txtMot.Enabled = false;
                    txtMot.Text = "";
                    for(int k = 0; k < mot.Length; k++)
                    {
                        txtMot.Text += "-";
                    }
                    // active les boutons et se positionne sur le premier
                    activeBoutons(true);
                }
                else
                {
                    // le mot n'est pas correct : efface la zone
                    MessageBox.Show("Le mot ne doit comporter que des lettres alphabétiques (pas d'espace, pas d'accent)");
                    txtMot.Text = "";
                    txtMot.Focus();
                }
            }
        }

        public frmPendu()
        {
            InitializeComponent();
        }

        /**
         * Evénement clic sur le bouton rejouer
         */
        private void btnRejouer_Click(object sender, EventArgs e)
        {
            debutJeu();
        }
    }
}
