﻿/**
 * tp 1 : morpion
 * author : Emds
 * date : 31/07/2017
 */
using System;

namespace tp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations et initialisations
            int essais = 0;     //compteur d'essais
            int[,] grille = new int[3, 3]; //matrice pour stocker les coups joués
            int joueur = 1;     //1 pour la premier joueur, 2 pour le second
            int l = 0, c = 0;   //numéro de ligne et de colonne
            bool gagne = false; //permet de savoir si un joueur a gagné

            // initialisation de la grille avec 10 dans chaque case
            // (10 pour une valeur supérieure à 3*2=6 
            // car la recherche du gagnant va se faire en faisant la somme
            // de chaque ligne, ou colonne, ou diagonale)
            for (int j = 0; j <= 2; j++)
            {
                for (int k = 0; k <= 2; k++)
                {
                    grille[j, k] = 10;
                }
            }

            // affichage de la grille vide
            Console.Clear();
            Console.SetCursorPosition(10, 5); Console.Write(" ----------- ");
            Console.SetCursorPosition(10, 6); Console.Write("|   |   |   |");
            Console.SetCursorPosition(10, 7); Console.Write(" ----------- ");
            Console.SetCursorPosition(10, 8); Console.Write("|   |   |   |");
            Console.SetCursorPosition(10, 9); Console.Write(" ----------- ");
            Console.SetCursorPosition(10, 10); Console.Write("|   |   |   |");
            Console.SetCursorPosition(10, 11); Console.Write(" ----------- ");

            // le jeu
            while (!gagne && essais < 9)
            {

                // saisie de la position et contrôle de saisie
                Console.SetCursorPosition(1, 15); Console.Write("C'est au tour du joueur "+joueur+" : ");
                do
                {
                    // capture d'erreur dans le cas où la saisie n'est pas un chiffre
                    try
                    {
                        Console.SetCursorPosition(10, 16); Console.Write("ligne   =    ");
                        Console.SetCursorPosition(10, 17); Console.Write("Colonne =    ");
                        Console.SetCursorPosition(20, 16);
                        l = int.Parse(Console.ReadKey().KeyChar.ToString());
                        Console.SetCursorPosition(20, 17);
                        c = int.Parse(Console.ReadKey().KeyChar.ToString());
                    }catch(Exception ex) { }
                // boucle tant que les chiffres saisis ne sont pas entre 1 et 3
                // ou que la case concernée n'est pas vide (contient 1 ou 2)
                } while (l < 1 || l > 3 || c < 1 || c > 3 || grille[l - 1, c - 1] < 3);

                // sauvegarde du nouveau coup joué
                grille[l - 1, c - 1] = joueur;

                // affichage du nouveau symbole dans la grille
                Console.SetCursorPosition(12 + (c - 1) * 4, 6 + (l - 1) * 2);
                if (joueur == 1)
                {
                    Console.Write("X");
                }
                else
                {
                    Console.Write("O");
                }

                // recherche d'un gagnant sur la ligne ou colonne 
                // qui contient le nouveau symbole
                // et recherche en même temps sur les 2 diagonales
                if (grille[l - 1, 0] + grille[l - 1, 1] + grille[l - 1, 2] == 3 * joueur ||
                    grille[0, c - 1] + grille[1, c - 1] + grille[2, c - 1] == 3 * joueur ||
                    grille[0, 0] + grille[1, 1] + grille[2, 2] == 3 * joueur ||
                    grille[0, 2] + grille[1, 1] + grille[2, 0] == 3 * joueur)
                {
                    gagne = true;
                }

                // compteur d'essais et changement de joueur
                essais++;
                if (!gagne)
                {
                    if (joueur == 1)
                    {
                        joueur = 2;
                    }
                    else
                    {
                        joueur = 1;
                    }
                }
            }

            // fin de la partie
            Console.SetCursorPosition(1, 20);
            if (gagne)
            {
                Console.WriteLine("le joueur " + joueur + " a gagné !");
            }
            else
            {
                Console.WriteLine("Egalité");
            }

            Console.ReadLine();
        }
    }
}
