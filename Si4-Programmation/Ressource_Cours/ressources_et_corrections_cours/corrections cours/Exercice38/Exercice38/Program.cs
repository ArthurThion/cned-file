﻿/**
 * Exercice 38 : trace d'un programme avec tableaux
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice38
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations
            int[] debut = new int[5];
            int[] fin = new int[5];

            // remmplissage du tablea udebut
            for(int val = 0; val < 5; val++)
            {
                debut[val] = (9 - val) % 5;
            }

            // remmplissage du tablea udebut
            for (int val = 0; val < 5; val++)
            {
                fin[4 - debut[val]] = val * 10;
            }

            // affichage de debut
            Console.Write("debut = ");
            for (int val = 0; val < 5; val++)
            {
                Console.Write(debut[val] + " ");
            }
            Console.WriteLine();

            // affichage de fin
            Console.Write("fin = ");
            for (int val = 0; val < 5; val++)
            {
                Console.Write(fin[val] + " ");
            }
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
