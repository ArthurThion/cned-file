﻿/**
 * Exercice 33 : tri par sélection
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice33
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre de cases
            Console.Write("Nombre de cases du vecteur = ");
            int nb = int.Parse(Console.ReadLine());

            // déclaration du vecteur
            int[] vec = new int[nb];

            // saisie du vecteur
            for(int k = 0; k < nb; k++)
            {
                Console.Write("saisir le nombre n°" + (k + 1) + " = ");
                vec[k] = int.Parse(Console.ReadLine());
            }

            // affichage de la première version du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write(vec[k] + " ");
            }
            Console.WriteLine();

            // tri par sélection et affichage
            for (int i = 0; i < nb - 1; i++)
            {
                int numcase = i;
                // recherche du plus petit élément sur le reste du vecteur
                for(int j = i + 1; j < nb; j++)
                {
                    if (vec[j] < vec[numcase])
                    {
                        numcase = j;
                    }
                }
                // échange des 2 cases
                int sauv = vec[numcase];
                vec[numcase] = vec[i];
                vec[i] = sauv;
                // affichage de la nouvelle version du vecteur
                for(int k = 0; k < nb; k++)
                {
                    Console.Write(vec[k] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
