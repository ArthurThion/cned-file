﻿/**
 * Exercice 52 : fonction moyenne d'un vecteur
 * author : Emds
 * date : 18/06/2017
 */
using System;

namespace Exercice52
{
    class Program
    {
        /**
         * Calul de la moyenne des valeurs dans un vecteur
         */
        static double moyVec(double[] vec)
        {
            double somme = 0;
            for (int k = 0; k < vec.Length; k++)
            {
                somme += vec[k];
            }
            return (somme / vec.Length);
        }

        static void Main(string[] args)
        {
            // Saisie du nombre de notes
            Console.Write("Entrez le nombre de notes = ");
            int nb = int.Parse(Console.ReadLine());

            // déclarations
            double[] lesNotes = new double[nb];

            // saisie des notes dans un vecteur 
            for (int k = 0; k < nb; k++)
            {
                Console.Write("Entrez la note n°"+(k+1)+" = ");
                lesNotes[k] = double.Parse(Console.ReadLine());

            }

            // affichage de la moyenne
            Console.WriteLine("Moyenne = " + moyVec(lesNotes));
            Console.ReadLine();
        }
    }
}
