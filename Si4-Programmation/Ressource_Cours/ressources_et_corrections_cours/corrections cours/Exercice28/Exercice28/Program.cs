﻿/**
 * Exercice 28 : conversion décimal en binaire
 * author : Emds
 * date : 30/05/2017
 */
using System;

namespace Exercice28
{
    class Program
    {
        static void Main(string[] args)
        {
            // Saisie d'un premier nombre en base 10 à convertir
            Console.Write("Entrez un entier (base 10) = ");
            int base10 = int.Parse(Console.ReadLine());

            // Boucle sur la saisie des nombres à convertir
            while (base10 != 0)
            {
                // Conversion par divisions successives
                string base2 = "";
                while (base10 != 0)
                {
                    base2 = (base10 % 2) + base2;
                    base10 = base10 / 2;
                }

                // Affichage du nombre en binaire
                Console.WriteLine("conversion en binaire = " + base2);

                // Saisie d'un nouveau nombre en base 10 à convertir
                Console.Write("Entrez un entier (base 10) = ");
                base10 = int.Parse(Console.ReadLine());
            }
        }
    }
}
