﻿/**
 * Exercice 61 : Récupération de fichiers
 * author : Emds
 * date : 11/07/2017
 */
using System;
using System.Windows.Forms;

namespace Exercice61
{
    public partial class Form1 : Form
    {
        // propriétés
        string dossier;

        public Form1()
        {
            InitializeComponent();
        }

        /**
         * Evénement clic sur bouton rechercher
         * Recherche un dossier et remplit la liste avec les fichiers
         */
        private void btnRechercher_Click(object sender, EventArgs e)
        {
            // boite de dialogue pour sélectionner un dossier
            FolderBrowserDialog rechercheDossier;
            rechercheDossier = new System.Windows.Forms.FolderBrowserDialog();
            rechercheDossier.Description = "Sélectionner un dossier";
            DialogResult choix = rechercheDossier.ShowDialog();
            // si un dossier est sélectionné
            if (choix == DialogResult.OK)
            {
                // récupération du chemin
                dossier = rechercheDossier.SelectedPath;
                // parcours des fichiers pour les insérer dans la liste
                lstFichiers.Items.Clear();
                foreach (string fichier in System.IO.Directory.GetFiles(dossier))
                {
                    string nom = System.IO.Path.GetFileName(fichier);
                    lstFichiers.Items.Add(nom);
                }
                majLabelNb();
            }
        }

        /**
         * Evénement clic sur btnVider
         * Permet de vider la liste lstSelection
         */
        private void btnVider_Click(object sender, EventArgs e)
        {
            lstSelection.Items.Clear();
            majLabelNb();
        }

        /**
         * Evénement clic sur btnSuppr
         * Permet de supprimer la ligne sélectionnée dans lstSelection
         */
        private void btnSuppr_Click(object sender, EventArgs e)
        {
            // contrôle si une ligne est sélectionnée
            if(lstSelection.SelectedIndex >= 0)
            {
                // suppression de la ligne
                lstSelection.Items.RemoveAt(lstSelection.SelectedIndex);
                majLabelNb();
            }
        }

        /**
         * Evénement clic sur btnAjouter
         * Ajoute dans la liste de droite la ligne sélectionnée dans la liste de gauche
         * si elle n'est pas déjà ajoutée
         */
        private void btnAjouter_Click(object sender, EventArgs e)
        {
            // contrôle si un élement est bien sélectionné dans la liste de gauche
            if(lstFichiers.SelectedIndex >= 0)
            {
                // regarde si le fichier n'est pas déjà présent dans la liste de droite
                if (lstSelection.FindStringExact(lstFichiers.SelectedItem.ToString()) < 0)
                {
                    // ajout du fichier dans la liste de droite
                    lstSelection.Items.Add(lstFichiers.SelectedItem);
                }
                majLabelNb();
            }
        }

        /**
         * Evénement clic sur btnTout
         * Transfère de toute la liste de gauche dans la liste de droite
         */
        private void btnTout_Click(object sender, EventArgs e)
        {
            lstSelection.Items.Clear();
            foreach(string ligne in lstFichiers.Items)
            {
                lstSelection.Items.Add(ligne);
            }
            majLabelNb();
        }

        /**
         * Mise à jour des 2 labels contenant le nombre de fichiers
         */
        private void majLabelNb()
        {
            lblNbFichiers.Text = lstFichiers.Items.Count + " fichier(s) dans le dossier";
            lblNbSelection.Text = lstSelection.Items.Count + " fichier(s) sélectionné(s)";
        }

        /**
         * Evénement chargement de la fenêtre
         * initialisations
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            // mise à jour de l'affichage du nombre de fichiers
            majLabelNb();
        }
    }
}
