﻿namespace Exercice61
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstFichiers = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRechercher = new System.Windows.Forms.Button();
            this.lstSelection = new System.Windows.Forms.ListBox();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.btnTout = new System.Windows.Forms.Button();
            this.lblNbFichiers = new System.Windows.Forms.Label();
            this.lblNbSelection = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnVider = new System.Windows.Forms.Button();
            this.btnSuppr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstFichiers
            // 
            this.lstFichiers.FormattingEnabled = true;
            this.lstFichiers.HorizontalScrollbar = true;
            this.lstFichiers.Location = new System.Drawing.Point(12, 33);
            this.lstFichiers.Name = "lstFichiers";
            this.lstFichiers.Size = new System.Drawing.Size(188, 212);
            this.lstFichiers.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "fichiers du dossier :";
            // 
            // btnRechercher
            // 
            this.btnRechercher.Location = new System.Drawing.Point(130, 6);
            this.btnRechercher.Name = "btnRechercher";
            this.btnRechercher.Size = new System.Drawing.Size(70, 21);
            this.btnRechercher.TabIndex = 2;
            this.btnRechercher.Text = "rechercher";
            this.btnRechercher.UseVisualStyleBackColor = true;
            this.btnRechercher.Click += new System.EventHandler(this.btnRechercher_Click);
            // 
            // lstSelection
            // 
            this.lstSelection.FormattingEnabled = true;
            this.lstSelection.HorizontalScrollbar = true;
            this.lstSelection.Location = new System.Drawing.Point(252, 33);
            this.lstSelection.Name = "lstSelection";
            this.lstSelection.Size = new System.Drawing.Size(188, 212);
            this.lstSelection.TabIndex = 3;
            // 
            // btnAjouter
            // 
            this.btnAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouter.Location = new System.Drawing.Point(206, 91);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(40, 40);
            this.btnAjouter.TabIndex = 4;
            this.btnAjouter.Text = ">";
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // btnTout
            // 
            this.btnTout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTout.Location = new System.Drawing.Point(206, 137);
            this.btnTout.Name = "btnTout";
            this.btnTout.Size = new System.Drawing.Size(40, 40);
            this.btnTout.TabIndex = 6;
            this.btnTout.Text = ">>";
            this.btnTout.UseVisualStyleBackColor = true;
            this.btnTout.Click += new System.EventHandler(this.btnTout_Click);
            // 
            // lblNbFichiers
            // 
            this.lblNbFichiers.AutoSize = true;
            this.lblNbFichiers.Location = new System.Drawing.Point(12, 253);
            this.lblNbFichiers.Name = "lblNbFichiers";
            this.lblNbFichiers.Size = new System.Drawing.Size(35, 13);
            this.lblNbFichiers.TabIndex = 9;
            this.lblNbFichiers.Text = "label2";
            // 
            // lblNbSelection
            // 
            this.lblNbSelection.AutoSize = true;
            this.lblNbSelection.Location = new System.Drawing.Point(258, 253);
            this.lblNbSelection.Name = "lblNbSelection";
            this.lblNbSelection.Size = new System.Drawing.Size(35, 13);
            this.lblNbSelection.TabIndex = 10;
            this.lblNbSelection.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(258, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "fichiers sélectionnés :";
            // 
            // btnVider
            // 
            this.btnVider.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVider.Image = global::Exercice61.Properties.Resources.vider;
            this.btnVider.Location = new System.Drawing.Point(446, 137);
            this.btnVider.Name = "btnVider";
            this.btnVider.Size = new System.Drawing.Size(40, 40);
            this.btnVider.TabIndex = 8;
            this.btnVider.Text = "clear all";
            this.btnVider.UseVisualStyleBackColor = true;
            this.btnVider.Click += new System.EventHandler(this.btnVider_Click);
            // 
            // btnSuppr
            // 
            this.btnSuppr.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuppr.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSuppr.Image = global::Exercice61.Properties.Resources.supprimer;
            this.btnSuppr.Location = new System.Drawing.Point(446, 91);
            this.btnSuppr.Name = "btnSuppr";
            this.btnSuppr.Size = new System.Drawing.Size(40, 40);
            this.btnSuppr.TabIndex = 7;
            this.btnSuppr.Text = "del";
            this.btnSuppr.UseVisualStyleBackColor = true;
            this.btnSuppr.Click += new System.EventHandler(this.btnSuppr_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 275);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblNbSelection);
            this.Controls.Add(this.lblNbFichiers);
            this.Controls.Add(this.btnVider);
            this.Controls.Add(this.btnSuppr);
            this.Controls.Add(this.btnTout);
            this.Controls.Add(this.btnAjouter);
            this.Controls.Add(this.lstSelection);
            this.Controls.Add(this.btnRechercher);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstFichiers);
            this.Name = "Form1";
            this.Text = "Fichiers";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstFichiers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRechercher;
        private System.Windows.Forms.ListBox lstSelection;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Button btnTout;
        private System.Windows.Forms.Button btnSuppr;
        private System.Windows.Forms.Button btnVider;
        private System.Windows.Forms.Label lblNbFichiers;
        private System.Windows.Forms.Label lblNbSelection;
        private System.Windows.Forms.Label label2;
    }
}

