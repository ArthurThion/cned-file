﻿/**
 * Exercice 11 : moyenne de 5 notes
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice11
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclarations
            float note, somme = 0;

            // Saisie des 5 notes et cumul    
            for (int k = 1; k <= 5; k++)
            {
                Console.Write("Entrez une note = ");
                note = float.Parse(Console.ReadLine());
                somme += note;
            }

            // Affichage de la moyenne
            Console.WriteLine("moyenne = " + (somme / 5));
            Console.ReadLine();
        }
    }
}
