﻿/**
 * Exercice 59 : Affichage de photos
 * author : Emds
 * date : 10/07/2017
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Exercice59
{
    public partial class frmPhoto : Form
    {
        // propriétés
        private string dossier;

        public frmPhoto()
        {
            InitializeComponent();
        }

        /**
         * Evénement clic sur le bouton btnDossier
         * Permet de sélectionner un dossier sur le disque
         * et de remplir la liste avec les fichiers du dossier
         */
        private void btnDossier_Click(object sender, EventArgs e)
        {
            // boite de dialogue pour sélectionner un dossier
            FolderBrowserDialog rechercheDossier;
            rechercheDossier = new System.Windows.Forms.FolderBrowserDialog();
            rechercheDossier.Description = "Sélectionner un dossier de photo";
            DialogResult choix = rechercheDossier.ShowDialog();
            // si un dossier est sélectionné
            if(choix == DialogResult.OK)
            {
                // récupération du chemin
                dossier = rechercheDossier.SelectedPath;
                // parcours des fichiers pour les insérer dans la liste
                lstPhoto.Items.Clear();
                foreach (string fichier in System.IO.Directory.GetFiles(dossier))
                {
                    string nom = System.IO.Path.GetFileName(fichier);
                    lstPhoto.Items.Add(nom);
                }
                // sélection du 1er élément de la liste
                if (lstPhoto.Items.Count > 0)
                {
                    lstPhoto.SelectedIndex = 0;
                    lstPhoto_SelectedIndexChanged(null, null);
                }
            }
        }

        /**
         * Evénement changement de sélection dans la liste lstPhoto
         * Affiche l'image sélectionnée si c'est possible
         */
        private void lstPhoto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string fichier = dossier + "\\" + lstPhoto.SelectedItem;
                pctPhoto.Image = Image.FromFile(fichier);
            }
            catch (Exception ex) {
                // en cas d'erreur, vider l'affichage
                pctPhoto.Image = null;
            }
        }

        /**
         * Evénement clic sur le bouton btnRotation
         * Permet de faire pivoter l'image
         */
        private void btnRotation_Click(object sender, EventArgs e)
        {
            try
            {
                pctPhoto.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                pctPhoto.Refresh();
            }
            catch (Exception ex) { };
        }
    }
}
