﻿/**
 * Exercice 47 : trace d'une procédure
 * author : Emds
 * date : 14/06/2017
 */
using System;

namespace Exercice47
{
    class Program
    {
        static void exemple(int a, ref int b, ref int c)
        {
            int d;
            b = a * 2;
            a = b - c;
            d = b - a;
            c = a * 2;
        }

        static void Main(string[] args)
        {
            int nbre1, nbre2, nbre3, d;
            nbre1 = 3;
            nbre2 = 2;
            nbre3 = 5;
            d = 8;
            exemple(nbre1, ref nbre2, ref nbre3);
            Console.WriteLine("nbre1 = " + nbre1);
            Console.WriteLine("nbre2 = " + nbre2);
            Console.WriteLine("nbre3 = " + nbre3);
            Console.WriteLine("d = " + d);
            Console.ReadLine();
        }
    }
}
