﻿/**
 * Exercice 62 : Convertisseur de monnaie
 * author : Emds
 * date : 12/07/2017
 */
using System;
using System.Windows.Forms;

namespace Exercice62
{
    public partial class frmConvertisseur : Form
    {
        // type
        struct sMonnaie
        {
            public string nom;
            public float valeur;
            public string signe;
        }
        // propriétés
        private sMonnaie[] tMonnaie = new sMonnaie[6];

        public frmConvertisseur()
        {
            InitializeComponent();
        }

        /**
         * Evénement chargement de la fenêtre
         * Initialisation des monnaies 
         */
        private void frmConvertisseur_Load(object sender, EventArgs e)
        {
            // initialisation du tableau de structure contenant les monnaies
            tMonnaie[0].nom = "Dollar américain";
            tMonnaie[0].valeur = 1.14185f;
            tMonnaie[0].signe = "$";
            tMonnaie[1].nom = "Livre sterling";
            tMonnaie[1].valeur = 0.88707f;
            tMonnaie[1].signe = "£";
            tMonnaie[2].nom = "Dinar";
            tMonnaie[2].valeur = 123.784f;
            tMonnaie[2].signe = "Dinar";
            tMonnaie[3].nom = "Mark";
            tMonnaie[3].valeur = 1.95583f;
            tMonnaie[3].signe = "DM";
            tMonnaie[4].nom = "Franc";
            tMonnaie[4].valeur = 6.55957f;
            tMonnaie[4].signe = "F";
            tMonnaie[5].nom = "Lire";
            tMonnaie[5].valeur = 1936.27f;
            tMonnaie[5].signe = "£";
            // remplissage du combo avec les monnaies
            for (int k=0; k<tMonnaie.Length; k++)
            {
                cboMonnaie.Items.Add(tMonnaie[k].nom);
            }
            // sélection de la 1ère monnaie
            if(cboMonnaie.Items.Count > 0)
            {
                cboMonnaie.SelectedIndex = 0;
            }
            // Initialisation à 1 euro
            txtEuro.Text = "1";
        }

        /**
         * Evénement changement de ligne dans cboMonnaie
         */
        private void cboMonnaie_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Affichage de la valeur correspondant à la monnaie choisie
            txtValeur.Text = tMonnaie[cboMonnaie.SelectedIndex].valeur.ToString();
            // Affichage du symbole de la monnaie dans le label
            lblMonnaie.Text = tMonnaie[cboMonnaie.SelectedIndex].signe;
            // Affichage de la conversion
            txtEuro_TextChanged(null, null);
        }

        /**
         * Evénement changement sur txtEuro
         * Mise à jour du résultat de la conversion
         */
        private void txtEuro_TextChanged(object sender, EventArgs e)
        {
            // tentative de récupération de la saisie en Euro
            try
            {
                float euro = float.Parse(txtEuro.Text);
                // affichage de la conversion dans la monnaie choisie
                txtMonnaie.Text = (euro * float.Parse(txtValeur.Text)).ToString();
            }catch(Exception ex)
            {
                txtEuro.Text = "";
            }
        }
    }
}
