﻿/**
 * Exercice 14 : plusieurs tables de multiplication
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration
            int entier;
            char reponse;

            // Boucle sur plusieurs tables de multiplications
            do
            {
                // Saisie d'un entier entre 1 et 9 avec contrôle de saisie
                do
                {
                    Console.Write("Entrez un entier entre 1 et 9  = ");
                    entier = int.Parse(Console.ReadLine());
                } while (entier < 1 || entier > 9);

                // boucle sur la table de multiplication de cet entier
                for (int k = 0; k <= 10; k++)
                {
                    Console.WriteLine(entier + " x " + k + " = " + (entier * k));
                }

                // Demande pour l'affichage d'une table de multiplication
                do
                {
                    Console.WriteLine();
                    Console.Write("Voulez-vous afficher une nouvelle table de multiplication ? (O/N) ");
                    reponse = Console.ReadKey().KeyChar;
                } while (reponse!='O' && reponse!='N');

                // Effacer l'écran
                Console.Clear();

            } while (reponse=='O');

        }
    }
}
