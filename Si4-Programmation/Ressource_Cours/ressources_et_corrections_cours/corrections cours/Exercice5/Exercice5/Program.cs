﻿/**
 * Exercice 5 : alternative (contrôle de l'âge : majeur/mineur)
 * author : Emds
 * date : 25/05/2017
 */
using System;

namespace Exercice5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration
            int age;

            // Saisie de l'âge
            Console.Write("Entrez votre âge = ");
            age = int.Parse(Console.ReadLine());

            // Test sur l'âge
            if (age >= 18)
            {
                Console.WriteLine("Vous êtes majeur");
            }
            else
            {
                Console.WriteLine("Vous êtes mineur");
                Console.WriteLine("Vous serez majeur dans " + (18 - age) + " an(s)");
            }
            Console.ReadLine();
        }
    }
}
