﻿/**
 * Exercice 17 : test sur la racine carrée
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice17
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre
            Console.Write("Entrez un nombre = ");
            int nombre = int.Parse(Console.ReadLine());

            // demande de la racine carrée
            Console.Write("Entrez la racine carrée = ");
            double racine = double.Parse(Console.ReadLine());

            // contrôle si la rasine carrée est correcte
            if (racine == Math.Sqrt(nombre))
            {
                Console.WriteLine("Bravo !");
            }
            else
            {
                Console.WriteLine("Erreur, la racine carrée de " + nombre + " est " + Math.Sqrt(nombre));
            }
            Console.ReadLine();
        }
    }
}
