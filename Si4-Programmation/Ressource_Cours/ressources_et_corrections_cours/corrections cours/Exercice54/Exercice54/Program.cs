﻿/**
 * Exercice 54 : signes du zodiaque
 * author : Emds
 * date : 19/06/2017
 */
using System;

namespace Exercice54
{
    class Program
    {
        public struct StrSigne
        {
            public string signe;
            public int jour;
            public int mois;
        }

        /**
         * saisie d'une reponse d'un caractère parmi 2
         */
        static char saisie(string message, char carac1, char carac2)
        {
            char reponse;
            do
            {
                Console.WriteLine();
                Console.Write(message + " (" + carac1 + "/" + carac2 + ") ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != carac1 && reponse != carac2);
            return reponse;
        }

        /**
         * recherche le signe correspondant au jour et mois passé en paramètre
         */
        static string monSigne(int j, int m)
        {
            // déclaration et initialisation du tableau des signes
            StrSigne[] lesSignes = new StrSigne[12];
            lesSignes[0].signe = "Verseau";
            lesSignes[0].jour = 21;
            lesSignes[0].mois = 1;
            lesSignes[1].signe = "Poisson";
            lesSignes[1].jour = 20;
            lesSignes[1].mois = 2;
            lesSignes[2].signe = "Bélier";
            lesSignes[2].jour = 21;
            lesSignes[2].mois = 3;
            lesSignes[3].signe = "Taureau";
            lesSignes[3].jour = 21;
            lesSignes[3].mois = 4;
            lesSignes[4].signe = "Gémeaux";
            lesSignes[4].jour = 22;
            lesSignes[4].mois = 5;
            lesSignes[5].signe = "Cancer";
            lesSignes[5].jour = 22;
            lesSignes[5].mois = 6;
            lesSignes[6].signe = "Lion";
            lesSignes[6].jour = 23;
            lesSignes[6].mois = 7;
            lesSignes[7].signe = "Vierge";
            lesSignes[7].jour = 23;
            lesSignes[7].mois = 8;
            lesSignes[8].signe = "Balance";
            lesSignes[8].jour = 23;
            lesSignes[8].mois = 9;
            lesSignes[9].signe = "Scorpion";
            lesSignes[9].jour = 21;
            lesSignes[9].mois = 10;
            lesSignes[10].signe = "Sagittaire";
            lesSignes[10].jour = 23;
            lesSignes[10].mois = 11;
            lesSignes[11].signe = "Capricorne";
            lesSignes[11].jour = 22;
            lesSignes[11].mois = 12;

            // recherche du signe
            int k = 0;
            while (k < 11 && !((j >= lesSignes[k].jour && m == lesSignes[k].mois) ||
                (j < lesSignes[k + 1].jour && m == lesSignes[k + 1].mois)))
            {
                k++;
            } ;

            // retour du signe
            return lesSignes[k].signe;
        }

        static void Main(string[] args)
        {
            // déclarations
            char reponse;

            // boucle sur la saisie du jour et du mois
            do
            {
                // saisie du jour et du mois
                Console.WriteLine();
                Console.Write("jour = ");
                int jour = int.Parse(Console.ReadLine());
                Console.Write("mois = ");
                int mois = int.Parse(Console.ReadLine());

                // affichage du signe correspondant
                Console.WriteLine("Votre signe est " + monSigne(jour, mois));

                // demande s'il faut continuer
                reponse = saisie("Voulez-vous continuer ? (O/N) ", 'O', 'N');

            } while (reponse == 'O');
        }
    }
}
