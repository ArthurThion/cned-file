﻿/**
 * Exercice 6 : alternative (affichage de la mention d'une moyenne)
 * author : Emds
 * date : 25/05/2017
 */
using System;

namespace Exercice6
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration
            float moyenne;

            // Saisie de la moyenne
            Console.Write("Entrez la moyenne = ");
            moyenne = float.Parse(Console.ReadLine());

            // Test de la moyenne et affichage de la mention
            if (moyenne < 10)
            {
                Console.WriteLine("recallé");
            }
            else
            {
                if (moyenne < 12)
                {
                    Console.WriteLine("passable");
                }
                else
                {
                    if (moyenne < 14)
                    {
                        Console.WriteLine("assez bien");
                    }
                    else
                    {
                        if (moyenne < 16)
                        {
                            Console.WriteLine("bien");
                        }
                        else
                        {
                            Console.WriteLine("très bien");
                        }
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
