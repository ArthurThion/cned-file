﻿/**
 * Exercice 40 : températures
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice40
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre de cases
            Console.Write("Nombre de températures = ");
            int nb = int.Parse(Console.ReadLine());

            // déclarations
            int[] frequence = new int[61];
            int temperature;

            // initialisation du tableau de fréquences
            for(int k = 0; k < 61; k++)
            {
                frequence[k] = 0;
            }

            // saisie des températures
            for (int k = 0; k < nb; k++)
            {
                // saisie avec contrôle entre -20 et 40
                do
                {
                    Console.Write("saisir la température n°" + (k + 1) + " = ");
                    temperature = int.Parse(Console.ReadLine());
                } while (temperature < -20 || temperature > 40);
                // calcul de la fréquence
                frequence[temperature + 20]++;
            }

            // affichage des fréquences
            Console.WriteLine();
            for(int k = 0; k < 61; k++)
            {
                if (frequence[k] != 0)
                {
                    Console.WriteLine("la température " + (k - 20) + " est apparue " + frequence[k] + " fois");
                }
            }
            Console.ReadLine();
        }
    }
}
