﻿/**
 * Exercice 49 : calcul d'une distance avec une procédure
 * author : Emds
 * date : 16/06/2017
 */
using System;

namespace Exercice49
{
    class Program
    {
        /**
         * Calcul de la distance entre 2 nombres
         */
        static void distance(int val1, int val2, ref int result)
        {
            if (val1 > val2)
            {
                result = val1 - val2;
            }
            else
            {
                result = val2 - val1;
            }
        }

        static void Main(string[] args)
        {
            // saisie des 2 nombres
            Console.Write("Entrez le premier nombre entier = ");
            int nb1 = int.Parse(Console.ReadLine());
            Console.Write("Entrez le second nombre entier = ");
            int nb2 = int.Parse(Console.ReadLine());

            // calcul de la distance
            int ladistance = 0;
            distance(nb1, nb2, ref ladistance);

            // affichage de la distance
            Console.WriteLine("Distance entre " + nb1 + " et " + nb2 + " = " + ladistance);
            Console.ReadLine();
        }
    }
}
