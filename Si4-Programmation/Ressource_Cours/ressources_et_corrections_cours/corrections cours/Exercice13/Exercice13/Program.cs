﻿/**
 * Exercice 13 : table de multiplication d'un entier saisi
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice13
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration
            int entier;

            // Saisie d'un entier entre 1 et 9 avec contrôle de saisie
            do
            {
                Console.Write("Entrez un entier entre 1 et 9 = ");
                entier = int.Parse(Console.ReadLine());
            } while (entier < 1 || entier > 9);

            // boucle sur la table de multiplication de cet entier
            for (int k = 0; k <= 10; k++)
            {
                Console.WriteLine(entier+" x " + k + " = " + (entier * k));
            }
            Console.ReadLine();
        }
    }
}
