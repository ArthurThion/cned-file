﻿/**
 * Exercice 12 : table de multiplication de 3
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice12
{
    class Program
    {
        static void Main(string[] args)
        {
            // boucle sur la table de multiplication
            for(int k = 0; k <= 10; k++)
            {
                Console.WriteLine("3 x " + k + " = " + (3 * k));
            }
            Console.ReadLine();
        }
    }
}
