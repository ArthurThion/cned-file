﻿namespace Exercice63
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNb1 = new System.Windows.Forms.TextBox();
            this.txtNb2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtReponse = new System.Windows.Forms.TextBox();
            this.lblBonneReponse = new System.Windows.Forms.Label();
            this.btnSuivant = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grbQuestion = new System.Windows.Forms.GroupBox();
            this.btnRecommencer = new System.Windows.Forms.Button();
            this.txtMaxNote = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.grbQuestion.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNote
            // 
            this.txtNote.Enabled = false;
            this.txtNote.Location = new System.Drawing.Point(6, 19);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(27, 20);
            this.txtNote.TabIndex = 4;
            this.txtNote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "/";
            // 
            // txtNb1
            // 
            this.txtNb1.Enabled = false;
            this.txtNb1.Location = new System.Drawing.Point(6, 19);
            this.txtNb1.Name = "txtNb1";
            this.txtNb1.Size = new System.Drawing.Size(27, 20);
            this.txtNb1.TabIndex = 6;
            this.txtNb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNb2
            // 
            this.txtNb2.Enabled = false;
            this.txtNb2.Location = new System.Drawing.Point(59, 19);
            this.txtNb2.Name = "txtNb2";
            this.txtNb2.Size = new System.Drawing.Size(27, 20);
            this.txtNb2.TabIndex = 7;
            this.txtNb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(92, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "=";
            // 
            // txtReponse
            // 
            this.txtReponse.Location = new System.Drawing.Point(116, 19);
            this.txtReponse.Name = "txtReponse";
            this.txtReponse.Size = new System.Drawing.Size(27, 20);
            this.txtReponse.TabIndex = 10;
            this.txtReponse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtReponse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReponse_KeyPress);
            // 
            // lblBonneReponse
            // 
            this.lblBonneReponse.AutoSize = true;
            this.lblBonneReponse.Location = new System.Drawing.Point(3, 50);
            this.lblBonneReponse.Name = "lblBonneReponse";
            this.lblBonneReponse.Size = new System.Drawing.Size(45, 13);
            this.lblBonneReponse.TabIndex = 12;
            this.lblBonneReponse.Text = "...x...=...";
            // 
            // btnSuivant
            // 
            this.btnSuivant.Location = new System.Drawing.Point(86, 45);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(57, 23);
            this.btnSuivant.TabIndex = 14;
            this.btnSuivant.Text = "suivante";
            this.btnSuivant.UseVisualStyleBackColor = true;
            this.btnSuivant.Click += new System.EventHandler(this.btnSuivant_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMaxNote);
            this.groupBox1.Controls.Add(this.txtNote);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(86, 46);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Note";
            // 
            // grbQuestion
            // 
            this.grbQuestion.Controls.Add(this.txtNb1);
            this.grbQuestion.Controls.Add(this.txtNb2);
            this.grbQuestion.Controls.Add(this.btnSuivant);
            this.grbQuestion.Controls.Add(this.label5);
            this.grbQuestion.Controls.Add(this.lblBonneReponse);
            this.grbQuestion.Controls.Add(this.label6);
            this.grbQuestion.Controls.Add(this.txtReponse);
            this.grbQuestion.Location = new System.Drawing.Point(12, 12);
            this.grbQuestion.Name = "grbQuestion";
            this.grbQuestion.Size = new System.Drawing.Size(154, 79);
            this.grbQuestion.TabIndex = 16;
            this.grbQuestion.TabStop = false;
            this.grbQuestion.Text = "Question";
            // 
            // btnRecommencer
            // 
            this.btnRecommencer.Location = new System.Drawing.Point(117, 97);
            this.btnRecommencer.Name = "btnRecommencer";
            this.btnRecommencer.Size = new System.Drawing.Size(49, 46);
            this.btnRecommencer.TabIndex = 17;
            this.btnRecommencer.Text = "Refaire le test";
            this.btnRecommencer.UseVisualStyleBackColor = true;
            this.btnRecommencer.Click += new System.EventHandler(this.btnRecommencer_Click);
            // 
            // txtMaxNote
            // 
            this.txtMaxNote.Enabled = false;
            this.txtMaxNote.Location = new System.Drawing.Point(53, 19);
            this.txtMaxNote.Name = "txtMaxNote";
            this.txtMaxNote.Size = new System.Drawing.Size(27, 20);
            this.txtMaxNote.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(178, 153);
            this.Controls.Add(this.btnRecommencer);
            this.Controls.Add(this.grbQuestion);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Tables";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grbQuestion.ResumeLayout(false);
            this.grbQuestion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNb1;
        private System.Windows.Forms.TextBox txtNb2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtReponse;
        private System.Windows.Forms.Label lblBonneReponse;
        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grbQuestion;
        private System.Windows.Forms.Button btnRecommencer;
        private System.Windows.Forms.TextBox txtMaxNote;
    }
}

