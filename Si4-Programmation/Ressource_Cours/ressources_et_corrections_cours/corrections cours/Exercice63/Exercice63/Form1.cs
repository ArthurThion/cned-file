﻿/**
 * Exercice 63 : Tables de multiplication
 * author : Emds
 * date : 14/07/2017
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Exercice63
{
    public partial class Form1 : Form
    {
        // propriétés
        private int nbQuestions = 20;
        private int numQuestion;
        private int note;

        public Form1()
        {
            InitializeComponent();
        }

        /**
         * Evénement clic sur btnRecommencer
         * Recommence une série de questions
         */
        private void btnRecommencer_Click(object sender, EventArgs e)
        {
            // initialisations
            numQuestion = 0;
            note = 0;
            txtNote.Text = note.ToString();
            txtMaxNote.Text = numQuestion.ToString();
            // lancer une question
            btnSuivant_Click(null, null);
        }

        /**
         * Evénement chargement de la fenêtre
         * démarrer un test (idem bouton btnRecommencer)
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            btnRecommencer_Click(null, null);
        }

        /**
         * Evénement clic sur btnSuivant
         * Passage à une nouvelle question
         */
        private void btnSuivant_Click(object sender, EventArgs e)
        {
            // désactiver le bouton suivant
            btnSuivant.Enabled = false;

            // incrémentation du compteur de questions
            numQuestion++;
            grbQuestion.Text = "Question n°" + numQuestion + "/" + nbQuestions;

            // remet à vide et blanc la zone de réponse
            txtReponse.BackColor = Color.Empty;
            txtReponse.Text = "";

            // fait disparaitre la bonne réponse
            lblBonneReponse.Visible = false;

            // initialisations de 2 nombres aléatoires entre 1 et 9
            Random nbAlea = new Random();
            txtNb1.Text = nbAlea.Next(1, 9).ToString();
            txtNb2.Text = nbAlea.Next(1, 9).ToString();

            // positionnement sur la zone de saisie de la réponse
            txtReponse.Focus();
        }

        /**
         * Evénement touche pressée dans txtReponse
         * contrôle si la réponse est correcte
         */
        private void txtReponse_KeyPress(object sender, KeyPressEventArgs e)
        {
            // si la touche est validation
            if(e.KeyChar == (char)Keys.Enter)
            {
                // calcul de la bonne réponse
                int bonneReponse = int.Parse(txtNb1.Text) * int.Parse(txtNb2.Text);
                // contrôle de la réponse
                if (txtReponse.Text == bonneReponse.ToString())
                {
                    txtReponse.BackColor = Color.LightGreen;
                    note++;
                }
                else
                {
                    txtReponse.BackColor = Color.LightSalmon;
                    // affichage de la bonne réponse
                    lblBonneReponse.Visible = true;
                    lblBonneReponse.Text = txtNb1.Text + " x " + txtNb2.Text + " = " + bonneReponse;
                }
                // affichage de la note 
                txtNote.Text = note.ToString();
                txtMaxNote.Text = numQuestion.ToString();
                // positionnement sur le bouton suivant ou rejouer
                if (numQuestion == nbQuestions)
                {
                    btnRecommencer.Focus();
                }
                else
                {
                    btnSuivant.Enabled = true;
                    btnSuivant.Focus();
                }
            }
        }
    }
}
