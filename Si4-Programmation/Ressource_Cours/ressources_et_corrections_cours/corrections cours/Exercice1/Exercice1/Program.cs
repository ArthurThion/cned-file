﻿/**
 * Exercice 1 : déclaration, initialisation et affichage personnalisé
 * author : Emds
 * date : 24/05/2017
 */
using System;

namespace Exercice1
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations et initialisations
            string prenom = "Alain";
            string nom = "DUPONT";
            int age = 19;

            // affichage personnalisé
            Console.WriteLine("Bonjour " + prenom + " " + nom + ", vous avez "+age+" ans.");
            Console.ReadLine();
        }
    }
}
