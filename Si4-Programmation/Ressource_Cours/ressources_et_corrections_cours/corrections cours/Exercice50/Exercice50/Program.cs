﻿/**
 * Exercice 50 : fonction valeur absolue
 * author : Emds
 * date : 18/06/2017
 */
using System;

namespace Exercice50
{
    class Program
    {
        /**
         * Valeur absolue d'un entier (sa partie non signée)
         */
        static int abs(int n)
        {
            if (n < 0)
            {
                return -n;
            }
            else
            {
                return n;
            }
        }

        static void Main(string[] args)
        {
            // saisie d'un entier
            Console.Write("entrez un nombre entier = ");
            int nombre = int.Parse(Console.ReadLine());

            // affichage de sa valeur absolue
            Console.WriteLine("La valeur absolue de " + nombre + " est " + abs(nombre));
            Console.ReadLine();
        }
    }
}
