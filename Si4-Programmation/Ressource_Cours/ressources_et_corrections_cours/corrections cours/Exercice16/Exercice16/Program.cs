﻿/**
 * Exercice 16 : Récupération d'une partie de phrase à partir d'un mot
 * author : Emds
 * date : 28/05/2017
 */
using System;

namespace Exercice16
{
    class Program
    {
        static void Main(string[] args)
        {
            // Saisie d'une phrase
            Console.Write("Entrez une phrase = ");
            string phrase = Console.ReadLine();

            // Saisie d'un mot
            Console.Write("Entrez un mot présent dans la phrase = ");
            string mot = Console.ReadLine();

            // Recherche de la position mot dans la phrase
            int position = phrase.IndexOf(mot);

            // Teste si le mot a été trouvé
            if (position != -1)
            {
                // affichage de la phrase à partir de ce mot
                Console.WriteLine(phrase.Substring(position));
            }
            else
            {
                Console.WriteLine("le mot n'existe pas");
            }
            Console.ReadLine();
        }
    }
}
