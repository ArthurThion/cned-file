﻿/**
 * Exercice 39 : inversion de vecteur
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice39
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre de cases
            Console.Write("Nombre de cases du vecteur = ");
            int nb = int.Parse(Console.ReadLine());

            // déclaration du vecteur
            int[] vec = new int[nb];

            // saisie du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write("saisir le nombre n°" + (k + 1) + " = ");
                vec[k] = int.Parse(Console.ReadLine());
            }

            // inversion
            for(int k = 0; k < nb / 2; k++)
            {
                int sauv = vec[k];
                vec[k] = vec[nb - k - 1];
                vec[nb - k - 1] = sauv;
            }

            // affichage du vecteur final
            Console.WriteLine("Vecteur final : ");
            for(int k = 0; k < nb; k++)
            {
                Console.Write(vec[k] + " ");
            }
            Console.ReadLine();
        }
    }
}
