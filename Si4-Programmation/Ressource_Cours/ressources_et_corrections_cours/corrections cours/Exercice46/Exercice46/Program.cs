﻿/**
 * Exercice 46 : nombres amis
 * author : Emds
 * date : 13/06/2017
 */
using System;

namespace Exercice46
{
    class Program
    {
        /**
         * saisie d'une reponse d'un caractère parmi 2
         */
        static char saisie(string message, char carac1, char carac2)
        {
            char reponse;
            do
            {
                Console.WriteLine();
                Console.Write(message + " (" + carac1 + "/" + carac2 + ") ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != carac1 && reponse != carac2);
            return reponse;
        }

        /**
         * somme des chiffres que composent un entier
         */
        static int sommeChiffres(int nombre)
        {
            int somme = 0;
            while (nombre != 0)
            {
                somme += nombre % 10;
                nombre = nombre / 10;
            }
            return somme;
        }

        /**
         * contrôle si 2 nombres sont amis
         */
        static bool amis(int nb1, int nb2)
        {
            return (sommeChiffres(nb1) == sommeChiffres(nb2));
        }
        
        static void Main(string[] args)
        {
            // Déclarations
            char rep;
            int nb1, nb2;

            // Boucle sur la saisie des 2 nombres
            do
            {
                // saisie des 2 nombres
                Console.WriteLine();
                Console.Write("nombre 1 = ");
                nb1 = int.Parse(Console.ReadLine());
                Console.Write("nombre 2 = ");
                nb2 = int.Parse(Console.ReadLine());
                // contrôle s'ils sont amis
                if (amis(nb1, nb2))
                {
                    Console.WriteLine("les nombres " + nb1 + " et " + nb2 + " sont amis");
                }
                else
                {
                    Console.WriteLine("les nombres " + nb1 + " et " + nb2 + " ne sont pas amis");
                }
                // demande si 2 nouveaux nombres doivent être testés
                rep = saisie("2 nouveaux nombres à tester ?", 'O', 'N');
            } while (rep == 'O') ;

        }
    }
}
