﻿/**
 * Exercice 41 : villes
 * author : Emds
 * date : 05/06/2017
 */
using System;

namespace Exercice41
{
    class Program
    {
        // structure
        struct StrVille
        {
            public string nom;
            public int frequence;
        }

        static void Main(string[] args)
        {
            // saisie du nombre de villes
            Console.Write("Nombre de villes = ");
            int nb = int.Parse(Console.ReadLine());

            // déclarations
            StrVille[] tabVille = new StrVille[nb];
            int nbVilles = 0; //nombre de villes différentes
            string ville;

            // saisie des villes
            for (int k = 0; k < nb; k++)
            {
                // saisie de la ville
                Console.Write("saisir la ville n°" + (k + 1) + " = ");
                ville = Console.ReadLine();
                // cherche si la ville n'est pas déjà saisie
                int j = 0;
                while(j < nbVilles - 1 && tabVille[j].nom != ville)
                {
                    j++;
                }
                if(tabVille[j].nom == ville)
                {
                    // la ville est déjà présente, augmentation de la fréquence
                    tabVille[j].frequence++;
                }
                else
                {
                    // première saisie de la ville
                    tabVille[nbVilles].nom = ville;
                    tabVille[nbVilles].frequence = 1;
                    nbVilles++;
                }
            }

            // affichage des fréquences
            Console.WriteLine();
            for (int k = 0; k < nbVilles; k++)
            {
                Console.WriteLine(tabVille[k].nom + " = " + tabVille[k].frequence);
            }
            Console.ReadLine();
        }
    }
}
