﻿/**
 * Exercice 35 : tri par bulle
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice35
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre de cases
            Console.Write("Nombre de cases du vecteur = ");
            int nb = int.Parse(Console.ReadLine());

            // déclaration du vecteur
            int[] vec = new int[nb];

            // saisie du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write("saisir le nombre n°" + (k + 1) + " = ");
                vec[k] = int.Parse(Console.ReadLine());
            }

            // affichage de la première version du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write(vec[k] + " ");
            }
            Console.WriteLine();

            // variable echange pour savoir s'il n'y a plus d'échange
            bool echange;

            // boucle sur les parcours du vecteur (tant qu'il n'est pas trié)
            do
            {
                echange = false;
                // parcours et comparaison des cases 2 à 2
                for(int k = 1; k < nb; k++)
                {
                    // si 2 cases consécutives ne sont pas dans l'ordre
                    if (vec[k] < vec[k - 1])
                    {
                        int sauv = vec[k];
                        vec[k] = vec[k - 1];
                        vec[k - 1] = sauv;
                        echange = true;
                    }
                }
                // affichage de la nouvelle version du vecteur
                for (int k = 0; k < nb; k++)
                {
                    Console.Write(vec[k] + " ");
                }
                Console.WriteLine();
            } while (echange);
            Console.ReadLine();
        }
    }
}
