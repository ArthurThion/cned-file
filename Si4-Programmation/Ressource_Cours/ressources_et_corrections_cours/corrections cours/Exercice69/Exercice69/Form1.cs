﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercice69
{
    public partial class frmPersonnage : Form
    {

        public abstract class Personnage
        {
            // propriétés
            protected string login = "inconnu";

            /**
             * constructeur simple (inconnu)
             */
            public Personnage()
            {
            }

            /**
             * constructeur pour initialiser le login
             */
            public Personnage(string login)
            {
                this.login = login;
            }

            /**
             * permet de retourner les informations du personnage
             */
            public abstract string info();

        }

        public class Humain : Personnage
        {
            // propriétés
            private int vie;

            /**
             * constructeur initialise la vie
             */
            public Humain(int vie)
            {
                this.vie = vie;
            }

            /**
             * constructeur initialise le login et la vie
             */
            public Humain(string login, int vie) : base(login)
            {
                this.vie = vie;
            }

            /**
             * retourne le login et la vie
             */
            public override string info()
            {
                return  "Humain : " + login + " (" + vie + ")";
            }
        }

        public class Xmen : Personnage
        {
            // propriétés
            private string pouvoir;

            /**
             * constructeur initialise le pouvoir
             */
            public Xmen(string pouvoir)
            {
                this.pouvoir = pouvoir;
            }

            /**
             * constructeur initialise le login et le pouvoir
             */
            public Xmen(string login, string pouvoir) : base(login)
            {
                this.pouvoir = pouvoir;
            }

            /**
             * retourne le login et le pouvoir
             */
            public override string info()
            {
                return "X Men : " + login + " (" + pouvoir + ")";
            }
        }
        
        public frmPersonnage()
        {
            InitializeComponent();
        }

        // propriétés
        private Personnage[] lesPersonnages = new Personnage[100];
        private int nbPerso = 0;

        /**
         * change la visiblité des objets graphiques
         */
        private void changeVisible(bool humain)
        {
            lblPouvoir.Visible = !humain;
            cboPouvoir.Visible = !humain;
            lblVie.Visible = humain;
            nudVie.Visible = humain;
        }

        /**
         * mise à jour de la liste à partir du tableau
         */
        private void majListe()
        {
            // vider la liste
            lstPersonnage.Items.Clear();
            // remplir la liste
            for(int k = 0; k < nbPerso; k++)
            {
                lstPersonnage.Items.Add(lesPersonnages[k].info());
            }
        }

        /**
         * clic sur bouton radio humain
         */
        private void rdbHumain_CheckedChanged(object sender, EventArgs e)
        {
            changeVisible(true);
        }

        /**
         * clic sur bouton radio xmen
         */
        private void rdbXmen_CheckedChanged(object sender, EventArgs e)
        {
            changeVisible(false);
        }

        /**
         * chargement de la fenêtre
         */
        private void frmPersonnage_Load(object sender, EventArgs e)
        {
            rdbHumain.Checked = true;
            changeVisible(true);
            cboPouvoir.SelectedIndex = 0;
            txtLogin.Focus();
        }

        /**
         * clic sur ajouter
         */
        private void btnAjouter_Click(object sender, EventArgs e)
        {
            // ajout du personnage dans le tableau
            nbPerso++;
            if (rdbHumain.Checked)
            {
                // ajout d'un humain
                if (txtLogin.Text == "")
                {
                    lesPersonnages[nbPerso - 1] = new Humain((int)nudVie.Value);
                }
                else
                {
                    lesPersonnages[nbPerso - 1] = new Humain(txtLogin.Text, (int)nudVie.Value);
                }
            }
            else
            {
                // ajout d'un xmen
                if (txtLogin.Text == "")
                {
                    lesPersonnages[nbPerso - 1] = new Xmen(cboPouvoir.SelectedItem.ToString());
                }
                else
                {
                    lesPersonnages[nbPerso - 1] = new Xmen(txtLogin.Text, cboPouvoir.SelectedItem.ToString());
                }
            }
            // mise à jour de la liste
            majListe();
            // repositionnement sur login
            txtLogin.Text = "";
            txtLogin.Focus();

        }
    }
}
