﻿namespace Exercice69
{
    partial class frmPersonnage
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.nudVie = new System.Windows.Forms.NumericUpDown();
            this.lblVie = new System.Windows.Forms.Label();
            this.cboPouvoir = new System.Windows.Forms.ComboBox();
            this.lblPouvoir = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.rdbXmen = new System.Windows.Forms.RadioButton();
            this.rdbHumain = new System.Windows.Forms.RadioButton();
            this.lstPersonnage = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVie)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAjouter);
            this.groupBox1.Controls.Add(this.nudVie);
            this.groupBox1.Controls.Add(this.lblVie);
            this.groupBox1.Controls.Add(this.cboPouvoir);
            this.groupBox1.Controls.Add(this.lblPouvoir);
            this.groupBox1.Controls.Add(this.txtLogin);
            this.groupBox1.Controls.Add(this.lblLogin);
            this.groupBox1.Controls.Add(this.rdbXmen);
            this.groupBox1.Controls.Add(this.rdbHumain);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 107);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Créer un personnage : ";
            // 
            // btnAjouter
            // 
            this.btnAjouter.Location = new System.Drawing.Point(246, 68);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(53, 29);
            this.btnAjouter.TabIndex = 8;
            this.btnAjouter.Text = "ajouter";
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // nudVie
            // 
            this.nudVie.Location = new System.Drawing.Point(65, 77);
            this.nudVie.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudVie.Name = "nudVie";
            this.nudVie.ReadOnly = true;
            this.nudVie.Size = new System.Drawing.Size(52, 20);
            this.nudVie.TabIndex = 7;
            this.nudVie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudVie.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblVie
            // 
            this.lblVie.AutoSize = true;
            this.lblVie.Location = new System.Drawing.Point(6, 77);
            this.lblVie.Name = "lblVie";
            this.lblVie.Size = new System.Drawing.Size(30, 13);
            this.lblVie.TabIndex = 6;
            this.lblVie.Text = "vie : ";
            // 
            // cboPouvoir
            // 
            this.cboPouvoir.FormattingEnabled = true;
            this.cboPouvoir.Items.AddRange(new object[] {
            "télépathie",
            "télékinésie",
            "métamorphose",
            "magnétisme",
            "force",
            "électrokinésie"});
            this.cboPouvoir.Location = new System.Drawing.Point(65, 46);
            this.cboPouvoir.Name = "cboPouvoir";
            this.cboPouvoir.Size = new System.Drawing.Size(157, 21);
            this.cboPouvoir.TabIndex = 5;
            // 
            // lblPouvoir
            // 
            this.lblPouvoir.AutoSize = true;
            this.lblPouvoir.Location = new System.Drawing.Point(6, 49);
            this.lblPouvoir.Name = "lblPouvoir";
            this.lblPouvoir.Size = new System.Drawing.Size(48, 13);
            this.lblPouvoir.TabIndex = 4;
            this.lblPouvoir.Text = "pouvoir :";
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(65, 18);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(157, 20);
            this.txtLogin.TabIndex = 3;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(6, 21);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(35, 13);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "login :";
            // 
            // rdbXmen
            // 
            this.rdbXmen.AutoSize = true;
            this.rdbXmen.Location = new System.Drawing.Point(247, 50);
            this.rdbXmen.Name = "rdbXmen";
            this.rdbXmen.Size = new System.Drawing.Size(53, 17);
            this.rdbXmen.TabIndex = 1;
            this.rdbXmen.Text = "x men";
            this.rdbXmen.UseVisualStyleBackColor = true;
            this.rdbXmen.CheckedChanged += new System.EventHandler(this.rdbXmen_CheckedChanged);
            // 
            // rdbHumain
            // 
            this.rdbHumain.AutoSize = true;
            this.rdbHumain.Checked = true;
            this.rdbHumain.Location = new System.Drawing.Point(246, 22);
            this.rdbHumain.Name = "rdbHumain";
            this.rdbHumain.Size = new System.Drawing.Size(59, 17);
            this.rdbHumain.TabIndex = 0;
            this.rdbHumain.TabStop = true;
            this.rdbHumain.Text = "humain";
            this.rdbHumain.UseVisualStyleBackColor = true;
            this.rdbHumain.CheckedChanged += new System.EventHandler(this.rdbHumain_CheckedChanged);
            // 
            // lstPersonnage
            // 
            this.lstPersonnage.FormattingEnabled = true;
            this.lstPersonnage.Location = new System.Drawing.Point(12, 130);
            this.lstPersonnage.Name = "lstPersonnage";
            this.lstPersonnage.Size = new System.Drawing.Size(315, 225);
            this.lstPersonnage.TabIndex = 1;
            // 
            // frmPersonnage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 364);
            this.Controls.Add(this.lstPersonnage);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmPersonnage";
            this.Text = "Jeu";
            this.Load += new System.EventHandler(this.frmPersonnage_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVie)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.NumericUpDown nudVie;
        private System.Windows.Forms.Label lblVie;
        private System.Windows.Forms.ComboBox cboPouvoir;
        private System.Windows.Forms.Label lblPouvoir;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.RadioButton rdbXmen;
        private System.Windows.Forms.RadioButton rdbHumain;
        private System.Windows.Forms.ListBox lstPersonnage;
    }
}

