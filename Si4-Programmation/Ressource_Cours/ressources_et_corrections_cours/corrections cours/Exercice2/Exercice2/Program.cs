﻿/**
 * Exercice 2 : déclaration, saisie et affichage personnalisé
 * author : Emds
 * date : 24/05/2017
 */
using System;

namespace Exercice2
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations
            string prenom, nom;
            int age;

            // saisie des 3 informations
            Console.Write("Entrez votre prénom : ");
            prenom = Console.ReadLine();
            Console.Write("Entrez votre nom : ");
            nom = Console.ReadLine();
            Console.Write("Entrez votre âge : ");
            age = int.Parse(Console.ReadLine());

            // affichage personnalisé
            Console.WriteLine("Bonjour " + prenom + " " + nom + ", vous avez " + age + " ans.");
            Console.ReadLine();
        }
    }
}
