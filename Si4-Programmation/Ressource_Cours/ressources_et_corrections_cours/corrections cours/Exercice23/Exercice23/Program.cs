﻿/**
 * Exercice 23 : min et max d'un ensemble de notes
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice23
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclaration
            int note;

            // saisie du nombre de notes
            Console.Write("Nombre de notes à saisir = ");
            int nbnotes = int.Parse(Console.ReadLine());

            // initialisation du min et max
            int min = 20, max = 0;

            // saisie des notes et recherche des min et max
            for (int k = 1; k <= nbnotes; k++)
            {
                // saisie de la noe
                Console.Write("note n°" + k + " = ");
                note = int.Parse(Console.ReadLine());
                // est-ce la nouvelle plus petite note ?
                if (note < min)
                {
                    min = note;
                }
                // est-ce la nouvelle plus grande note ?
                if (note > max)
                {
                    max = note;
                }
            }

            // affichage des extrema
            if (nbnotes > 0)
            {
                Console.WriteLine("La plus petite note est = " + min);
                Console.WriteLine("La plus grande note est = " + max);
            }
            Console.ReadLine();
        }
    }
}
