﻿/**
 * Exercice 19 : optimisation de code (montant avec remise)
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice19
{
    class Program
    {
        static void Main(string[] args)
        {
            // Saisie du montant
            Console.Write("Entrez un montant = ");
            float montant = float.Parse(Console.ReadLine());

            // recherche de la remise
            float remise = 0;
            if(montant > 40)
            {
                remise = 10;
            }
            else
            {
                if (montant >= 20)
                {
                    remise = 5;
                }
            }

            // affichage de la remise et du montant final
            Console.WriteLine("montant = " + (montant * (1 - remise / 100)) + " avec une remise de " + remise + "%");
            Console.ReadLine(); 
        }
    }
}
