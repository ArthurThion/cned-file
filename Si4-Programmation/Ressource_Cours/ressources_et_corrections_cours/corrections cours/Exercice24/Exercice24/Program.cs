﻿/**
 * Exercice 24 : min et max parmi 10 températures
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice24
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclaration
            double temperature, min, max;

            // saisie de la 1ère température pour initialiser min et max
            Console.Write("température n°1 = ");
            min = max = double.Parse(Console.ReadLine());

            // saisie des autres températures et recherche des min et max
            for (int k = 2; k <= 10; k++)
            {
                // saisie de la temperature
                Console.Write("temperature n°" + k + " = ");
                temperature = double.Parse(Console.ReadLine());
                // est-ce la nouvelle plus petite température ?
                if (temperature < min)
                {
                    min = temperature;
                }
                // est-ce la nouvelle plus grande température ?
                if (temperature > max)
                {
                    max = temperature;
                }
            }

            // affichage des extrema
            Console.WriteLine("La plus petite température est = " + min);
            Console.WriteLine("La plus grande température est = " + max);
            Console.ReadLine();
        }
    }
}
