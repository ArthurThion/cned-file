﻿/**
 * Exercice 26 : transformation secondes en HH:MM:SS
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice26
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations
            int secondes; // nombre de secondes à convertir
            string heure; // heure finale correspondante, sous forme "HH:MM:SS"

            // saisie des secondes avec conrôle de saisie
            do
            {
                Console.Write("Enrez le nombre de secondes (entre 0 et 86400) = ");
                secondes = int.Parse(Console.ReadLine());
            } while (secondes < 0 || secondes > 86400);

            // calcul des heures, minutes, secondes correspondantes
            int h = secondes / 3600;
            int m = (secondes - (h * 3600)) / 60;
            int s = secondes - (h * 3600) - (m * 60);

            // formatage de la chaine à afficher (avec ajout de "0" si nécessaire
            heure = "";
            if (h<10)
            {
                heure += "O";
            }
            heure += h + ":";
            if (m < 10)
            {
                heure += "O";
            }
            heure += m + ":";
            if (s < 10)
            {
                heure += "O";
            }
            heure += s;

            // affichage final de l'heure
            Console.WriteLine(heure);
            Console.ReadLine();
        }
    }
}
