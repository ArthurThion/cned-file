﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice27
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclaration
            int nombre;

            // saisie du 1er nombre
            Console.Write("Entrez un entier positif (0 pour arrêter) = ");
            nombre = int.Parse(Console.ReadLine());

            // boucle sur les nombres
            while (nombre != 0)
            {
                // recherche si le nombre est premier
                int diviseur = 2;
                while((nombre % diviseur != 0) && (diviseur<Math.Sqrt(nombre)))
                {
                    diviseur++;
                }

                // test si on est sorti de la boucle sans trouver de diviseur ou si nombre = diviseur (cas particulier pour 1 et 2)
                if (nombre % diviseur != 0 || nombre == diviseur)
                {
                    Console.WriteLine(nombre + " est premier");
                }
                else
                {
                    Console.WriteLine(nombre + " n'est pas premier");
                }

                // saisie d'un nombre
                Console.Write("Entrez un entier positif (0 pour arrêter) = ");
                nombre = int.Parse(Console.ReadLine());
            }
        }
    }
}
