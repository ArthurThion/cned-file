﻿/**
 * Exercice 48 : trace de fonctions
 * author : Emds
 * date : 15/06/2017
 */
using System;

namespace Exercice48
{
    class Program
    {
        static int calcul2(int x)
        {
            x = 2 * x;
            return (x - 2);
        }

        static int calcul1(ref int a, int b)
        {
            a = 3 * b - a;
            return (calcul2(a) - 4);
        }

        static void Main(string[] args)
        {
            int a = 1;
            int b = 5;
            int c = calcul2(b) - b;
            int d = calcul1(ref a, calcul1(ref b, c)) - 2;
            Console.WriteLine("a = " + a);
            Console.WriteLine("b = " + b);
            Console.WriteLine("c = " + c);
            Console.WriteLine("d = " + d);
            Console.ReadLine();
        }
    }
}
