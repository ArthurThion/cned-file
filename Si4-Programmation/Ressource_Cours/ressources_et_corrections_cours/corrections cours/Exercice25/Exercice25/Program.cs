﻿/**
 * Exercice 25 : suite strictement croissante
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice25
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations
            char reponse;
            bool croissant = true;
            double valeur, ancvaleur;

            // saisie d'une première valeur
            Console.Write("valeur = ");
            ancvaleur = double.Parse(Console.ReadLine());

            // question pour continuer
            Console.Write("Voulez-vous continuer ? (O/N) ");
            reponse = Console.ReadKey().KeyChar;

            // boucle sur la saisie des valeurs
            while (reponse == 'O')
            {
                // saisie d'une valeur
                Console.WriteLine();
                Console.Write("valeur = ");
                valeur = double.Parse(Console.ReadLine());

                // test de la valeur par rapport à la précédente 
                if (valeur <= ancvaleur)
                {
                    croissant = false;
                }

                // mémorisation de la valeur dans ancvaleur pour la comparaison suivante
                ancvaleur = valeur;

                // question pour continuer
                Console.Write("Voulez-vous continuer ? (O/N) ");
                reponse = Console.ReadKey().KeyChar;
            };

            // affichage du message final (suite croissante ou non)
            Console.WriteLine();
            if (croissant)
            {
                Console.WriteLine("OUI, la suite est strictement croissante");
            }
            else
            {
                Console.WriteLine("NON, la suite n'est pas strictement croissante");
            }
            Console.ReadLine();
        }
    }
}
