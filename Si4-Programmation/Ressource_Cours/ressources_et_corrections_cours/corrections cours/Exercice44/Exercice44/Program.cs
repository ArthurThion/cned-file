﻿/**
 * Exercice 44 : rêprise de l'exercice 43 en ajoutant une fonction de test de saisie de 2 caractères
 * author : Emds
 * date : 13/06/2017
 */
using System;

namespace Exercice44
{
    class Program
    {
        /**
         * saisie d'une reponse d'un caractère parmi 2
         */
        static char saisie(string message, char carac1, char carac2)
        {
            char reponse;
            do
            {
                Console.WriteLine();
                Console.Write(message + " (" + carac1 + "/" + carac2 + ") ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != carac1 && reponse != carac2);
            return reponse;
        }

        static void Main(string[] args)
        {
            // Déclarations
            float prix, total = 0;
            char rep;

            // demande si un prix est à saisir
            rep = saisie("Avez-vous un prix à saisir ?", 'O', 'N');

            // Boucle sur la saisie des prix et le cumul
            while (rep == 'O')
            {
                // saisie d'un nouveau prix
                Console.Write("   Entrez un prix = ");
                prix = float.Parse(Console.ReadLine());
                // cumul
                total = total + prix;
                // demande si un nouveau prix est à saisir
                rep = saisie("Avez-vous un prix à saisir ?", 'O', 'N');
            }

            // Affichage du total
            Console.WriteLine("   total des prix = " + total);
            Console.ReadLine();
        }
    }
}
