﻿/**
 * Trace : exemple simple d'itération pour gérer une exécution pas à pas
 * author : Emds
 * date : 26/05/2017
 */
using System;

namespace Trace
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclarations
            int somme = 0, k = 1;

            // Boucle pour cumuler les 5 premiers entiers
            while (k <= 5)
            {
                somme = somme + k;
                k++;
            }

            // Affichage du résultat
            Console.WriteLine("somme = " + somme);
            Console.ReadLine();
        }
    }
}
