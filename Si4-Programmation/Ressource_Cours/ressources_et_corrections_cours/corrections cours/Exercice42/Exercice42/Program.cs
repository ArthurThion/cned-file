﻿/**
 * Exercice 42 : reprise de l'exercice 8 en contrôlant la saisie O/N
 * author : Emds
 * date : 12/06/2017
 */
using System;

namespace Exercice42
{
    class Program
    {
        // déclarations globales
        static char reponse;

        /**
            * saisie d'une reponse O ou N
            */
        static void saisie()
        {
            do
            {
                Console.WriteLine();
                Console.Write("Avez-vous un prix à saisir ? (O/N) ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != 'O' && reponse != 'N');
        }

        static void Main(string[] args)
        {
            // Déclarations
            float prix, total = 0;

            // demande si un prix est à saisir
            saisie();

            // Boucle sur la saisie des prix et le cumul
            while (reponse == 'O')
            {
                // saisie d'un nouveau prix
                Console.Write("   Entrez un prix = ");
                prix = float.Parse(Console.ReadLine());
                // cumul
                total = total + prix;
                // demande si un nouveau prix est à saisir
                saisie();
            }

            // Affichage du total
            Console.WriteLine("   total des prix = " + total);
            Console.ReadLine();
        }
    }
}
