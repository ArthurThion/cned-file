﻿/**
 * Exercice 45 : rêprise de l'exercice 10 en utilisant la fonction de l'exercice 44
 * author : Emds
 * date : 13/06/2017
 */
using System;

namespace Exercice45
{
    class Program
    {
        /**
         * saisie d'une reponse d'un caractère parmi 2
         */
        static char saisie(string message, char carac1, char carac2)
        {
            char reponse;
            do
            {
                Console.WriteLine();
                Console.Write(message + " (" + carac1 + "/" + carac2 + ") ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != carac1 && reponse != carac2);
            return reponse;
        }

        static void Main(string[] args)
        {
            // Déclaration
            char sexe;

            // Saisie  du sexe
            sexe = saisie("Quel est votre sexe ? ", 'H', 'F');

            // Affichage du message personnalisé
            Console.WriteLine();
            if (sexe == 'H')
            {
                Console.WriteLine("Bonjour monsieur");
            }
            else
            {
                Console.WriteLine("Bonjour madame");
            }
            Console.ReadLine();
        }
    }
}
