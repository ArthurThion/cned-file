﻿/**
 * Exercice 7 : itération avec saisie de prix et affichage du total
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice7
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclarations
            float prix, total = 0;

            // Saisie d'un premier prix
            Console.Write("Entrez un prix (0 pour terminer) = ");
            prix = float.Parse(Console.ReadLine());

            // Boucle sur la saisie des prix et le cumul
            while (prix != 0)
            {
                total = total + prix;
                // saisie d'un nouveau prix
                Console.Write("Entrez un prix (0 pour terminer) = ");
                prix = float.Parse(Console.ReadLine());
            }

            // Affichage du total
            Console.WriteLine("total des prix = " + total);
            Console.ReadLine();
        }
    }
}
