﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercice66
{
    public partial class frmPerso : Form
    {

        public class Personnage
        {
            private string login;
            private int vie;
            /**
             * Constructeur
             * Valorise le login 
             * et initialise la vie aléatoirement entre 0 et 5
             */
            public Personnage(string login)
            {
                this.login = login;
                Random nbAlea = new Random();
                this.vie = nbAlea.Next(0, 5);
            }
            /**
             * Getter sur vie
             */
            public int getVie()
            {
                return vie;
            }
            /**
             * setter sur vie
             */
            public void setVie(int vie)
            {
                this.vie = vie;
            }
            /**
             * Getter sur login
             */
            public string getLogin()
            {
                return login;
            }
        }

        public frmPerso()
        {
            InitializeComponent();
        }

        // tableau de personnages
        private Personnage[] lesPersonnages = new Personnage[100];
        private int nbPerso = 0;

        /**
         * affichage du login suivi de la vie ou de "est mort"
         */
        private void afficheMessagePerso(Personnage unPersonnage)
        {
            if(unPersonnage.getVie() == 0)
            {
                txtEtatPerso.Text = unPersonnage.getLogin() + " est mort";
            }
            else
            {
                txtEtatPerso.Text = unPersonnage.getLogin() + " : " + unPersonnage.getVie();
            }
        }

        /**
         * mise à jour de la liste des personnages
         */
        private void majListePerso(int ligneActive)
        {
            // vider la liste
            lstPersonnage.Items.Clear();
            // remplir avec le tableau de personnages
            for(int k = 0; k < nbPerso; k++)
            {
                lstPersonnage.Items.Add(lesPersonnages[k].getLogin()+" ("+lesPersonnages[k].getVie()+")");
            }
            // positionnement sur la ligne active
            if (lstPersonnage.Items.Count > 0 && ligneActive < nbPerso)
            {
                lstPersonnage.SelectedIndex = ligneActive;
            }
        }

        /**
         * recherche du login corespondant à la vie max
         */
        private void recherchePersoMaxVie()
        {
            // contrôle s'il y a au moins un personnage
            if(nbPerso == 0)
            {
                txtPersoMaxVie.Text = "";
            }
            else
            {
                // recherche de l'indice de la vie max
                int max = 0;
                for(int k = 1; k < nbPerso; k++)
                {
                    if (lesPersonnages[k].getVie() > lesPersonnages[max].getVie())
                    {
                        max = k;
                    }
                }
                txtPersoMaxVie.Text = lesPersonnages[max].getLogin();
            }
        }

        /**
         * changement de vie
         */
        private void changeViePerso(int index)
        {
            // affichage du message perso
            afficheMessagePerso(lesPersonnages[index]);
            // mise à jour de la liste des personnages
            majListePerso(index);
            // affichage du login de la vie max
            recherchePersoMaxVie();
        }

        /**
         * touche appuyée sur txtLogin (contrôle de la validation)
         * création d'un nouveau personnage
         */
        private void txtLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            // contrôle si validation
            if(e.KeyChar == (char)Keys.Enter)
            {
                // incrémentation du nombre de personnages
                nbPerso++;
                // création du personnage
                Personnage unPersonnage = new Personnage(txtLogin.Text);
                // ajout du personnage dans le tableau
                lesPersonnages[nbPerso - 1] = unPersonnage;
                // mettre à jour tous les autres affichages
                changeViePerso(nbPerso - 1);
                // vider la zone de saisie et se repositionner dessus
                txtLogin.Text = "";
                txtLogin.Focus();
            }
        }

        /**
         * sélection d'une ligne dans la liste des personnages
         */
        private void lstPersonnage_SelectedIndexChanged(object sender, EventArgs e)
        {
            nudVie.Value = lesPersonnages[lstPersonnage.SelectedIndex].getVie();
        }

        /**
         * changement de la vie sur nudVie
         */
        private void nudVie_ValueChanged(object sender, EventArgs e)
        {
            // contrôle si une ligne est sélectionnée
            if (lstPersonnage.SelectedIndex >= 0)
            {
                // récupération du personnage sélectionné
                Personnage unPersonnage = lesPersonnages[lstPersonnage.SelectedIndex];
                // modification de la vie
                unPersonnage.setVie((int)nudVie.Value);
                changeViePerso(lstPersonnage.SelectedIndex);
            }
        }
    }
}
