﻿namespace Exercice66
{
    partial class frmPerso
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.lstPersonnage = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPersoMaxVie = new System.Windows.Forms.TextBox();
            this.txtEtatPerso = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nudVie = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudVie)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "login :";
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(52, 7);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(130, 20);
            this.txtLogin.TabIndex = 1;
            this.txtLogin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLogin_KeyPress);
            // 
            // lstPersonnage
            // 
            this.lstPersonnage.FormattingEnabled = true;
            this.lstPersonnage.Location = new System.Drawing.Point(15, 111);
            this.lstPersonnage.Name = "lstPersonnage";
            this.lstPersonnage.Size = new System.Drawing.Size(169, 134);
            this.lstPersonnage.TabIndex = 8;
            this.lstPersonnage.SelectedIndexChanged += new System.EventHandler(this.lstPersonnage_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Personnages :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Personnage ayant le plus de vie :";
            // 
            // txtPersoMaxVie
            // 
            this.txtPersoMaxVie.Enabled = false;
            this.txtPersoMaxVie.Location = new System.Drawing.Point(12, 280);
            this.txtPersoMaxVie.Name = "txtPersoMaxVie";
            this.txtPersoMaxVie.Size = new System.Drawing.Size(168, 20);
            this.txtPersoMaxVie.TabIndex = 11;
            // 
            // txtEtatPerso
            // 
            this.txtEtatPerso.Enabled = false;
            this.txtEtatPerso.Location = new System.Drawing.Point(12, 64);
            this.txtEtatPerso.Name = "txtEtatPerso";
            this.txtEtatPerso.Size = new System.Drawing.Size(170, 20);
            this.txtEtatPerso.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "vie :";
            // 
            // nudVie
            // 
            this.nudVie.Location = new System.Drawing.Point(52, 38);
            this.nudVie.Name = "nudVie";
            this.nudVie.ReadOnly = true;
            this.nudVie.Size = new System.Drawing.Size(57, 20);
            this.nudVie.TabIndex = 6;
            this.nudVie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudVie.ValueChanged += new System.EventHandler(this.nudVie_ValueChanged);
            // 
            // frmPerso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(195, 311);
            this.Controls.Add(this.txtEtatPerso);
            this.Controls.Add(this.txtPersoMaxVie);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lstPersonnage);
            this.Controls.Add(this.nudVie);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.label1);
            this.Name = "frmPerso";
            this.Text = "Perso";
            ((System.ComponentModel.ISupportInitialize)(this.nudVie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.ListBox lstPersonnage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPersoMaxVie;
        private System.Windows.Forms.TextBox txtEtatPerso;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudVie;
    }
}

