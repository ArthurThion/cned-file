﻿/**
 * Exercice 3 : calcul de moyenne
 * author : Emds
 * date : 24/05/2017
 */
using System;

namespace Exercice3
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations
            float totalNotes, moyenne;
            int nbNotes;

            // saisie des informations
            Console.Write("Entrez la somme des notes = ");
            totalNotes = float.Parse(Console.ReadLine());
            Console.Write("Entrez le nombre de notes = ");
            nbNotes = int.Parse(Console.ReadLine());

            // calcul de la moyenne
            moyenne = totalNotes / nbNotes;

            // affichage de la moyenne
            Console.WriteLine("Moyenne = " + moyenne);
            Console.ReadLine();
        }
    }
}
