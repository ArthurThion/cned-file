﻿/**
 * Exercice 36 : recherche séquentielle dans un vecteur
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice36
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre de cases
            Console.Write("Nombre de cases du vecteur = ");
            int nb = int.Parse(Console.ReadLine());

            // déclaration du vecteur
            int[] vec = new int[nb];

            // saisie du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write("saisir le nombre n°" + (k + 1) + " = ");
                vec[k] = int.Parse(Console.ReadLine());
            }

            // saisie de la valeur à chercher
            Console.Write("Valeur à chercher = ");
            int valeur = int.Parse(Console.ReadLine());

            // recherche de la position
            int position = 0;
            while(position < nb - 1 && vec[position] != valeur)
            {
                position++;
            }

            // test si la valeur a été trouvée
            if(vec[position] == valeur)
            {
                Console.WriteLine(valeur + " a été trouvé à la position " + (position + 1));
            }
            else
            {
                Console.WriteLine(valeur + " n'a pas été trouvé");
            }
            Console.ReadLine();
        }
    }
}
