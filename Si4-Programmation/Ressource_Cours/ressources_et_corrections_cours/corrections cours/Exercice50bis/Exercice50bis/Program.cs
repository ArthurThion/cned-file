﻿/**
 * Exercice 50 bis : procédure valeur absolue
 * author : Emds
 * date : 18/06/2017
 */
using System;

namespace Exercice50bis
{
    class Program
    {
        /**
         * Valeur absolue d'un entier (sa partie non signée)
         */
        static void abs(int n, ref int result)
        {
            if (n < 0)
            {
                result = -n;
            }
            else
            {
                result = n;
            }
        }

        static void Main(string[] args)
        {
            // saisie d'un entier
            Console.Write("entrez un nombre entier = ");
            int nombre = int.Parse(Console.ReadLine());

            // récupération de la valeur absolue
            int absNombre = 0;
            abs(nombre, ref absNombre);

            // affichage de la valeur absolue
            Console.WriteLine("La valeur absolue de " + nombre + " est " + absNombre);
            Console.ReadLine();
        }
    }
}
