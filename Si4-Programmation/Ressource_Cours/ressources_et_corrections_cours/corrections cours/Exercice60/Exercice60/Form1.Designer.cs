﻿namespace Exercice60
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTaille = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbLarge = new System.Windows.Forms.RadioButton();
            this.rdbNormale = new System.Windows.Forms.RadioButton();
            this.rdbGracile = new System.Windows.Forms.RadioButton();
            this.dtpNaiss = new System.Windows.Forms.DateTimePicker();
            this.btnCalcul = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPoidsIdeal = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "taille (en cm) :";
            // 
            // txtTaille
            // 
            this.txtTaille.Location = new System.Drawing.Point(90, 6);
            this.txtTaille.Name = "txtTaille";
            this.txtTaille.Size = new System.Drawing.Size(116, 20);
            this.txtTaille.TabIndex = 1;
            this.txtTaille.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaille.TextChanged += new System.EventHandler(this.txtTaille_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "date naiss :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbLarge);
            this.groupBox1.Controls.Add(this.rdbNormale);
            this.groupBox1.Controls.Add(this.rdbGracile);
            this.groupBox1.Location = new System.Drawing.Point(15, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 43);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "morphologie";
            // 
            // rdbLarge
            // 
            this.rdbLarge.AutoSize = true;
            this.rdbLarge.Location = new System.Drawing.Point(136, 19);
            this.rdbLarge.Name = "rdbLarge";
            this.rdbLarge.Size = new System.Drawing.Size(48, 17);
            this.rdbLarge.TabIndex = 2;
            this.rdbLarge.Text = "large";
            this.rdbLarge.UseVisualStyleBackColor = true;
            this.rdbLarge.CheckedChanged += new System.EventHandler(this.rdbLarge_CheckedChanged);
            // 
            // rdbNormale
            // 
            this.rdbNormale.AutoSize = true;
            this.rdbNormale.Checked = true;
            this.rdbNormale.Location = new System.Drawing.Point(68, 19);
            this.rdbNormale.Name = "rdbNormale";
            this.rdbNormale.Size = new System.Drawing.Size(62, 17);
            this.rdbNormale.TabIndex = 1;
            this.rdbNormale.TabStop = true;
            this.rdbNormale.Text = "normale";
            this.rdbNormale.UseVisualStyleBackColor = true;
            this.rdbNormale.CheckedChanged += new System.EventHandler(this.rdbNormale_CheckedChanged);
            // 
            // rdbGracile
            // 
            this.rdbGracile.AutoSize = true;
            this.rdbGracile.Location = new System.Drawing.Point(6, 19);
            this.rdbGracile.Name = "rdbGracile";
            this.rdbGracile.Size = new System.Drawing.Size(56, 17);
            this.rdbGracile.TabIndex = 0;
            this.rdbGracile.Text = "gracile";
            this.rdbGracile.UseVisualStyleBackColor = true;
            this.rdbGracile.CheckedChanged += new System.EventHandler(this.rdbGracile_CheckedChanged);
            // 
            // dtpNaiss
            // 
            this.dtpNaiss.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNaiss.Location = new System.Drawing.Point(90, 32);
            this.dtpNaiss.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dtpNaiss.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpNaiss.Name = "dtpNaiss";
            this.dtpNaiss.Size = new System.Drawing.Size(116, 20);
            this.dtpNaiss.TabIndex = 5;
            this.dtpNaiss.ValueChanged += new System.EventHandler(this.dtpNaiss_ValueChanged);
            // 
            // btnCalcul
            // 
            this.btnCalcul.Location = new System.Drawing.Point(72, 107);
            this.btnCalcul.Name = "btnCalcul";
            this.btnCalcul.Size = new System.Drawing.Size(73, 20);
            this.btnCalcul.TabIndex = 6;
            this.btnCalcul.Text = "calculer";
            this.btnCalcul.UseVisualStyleBackColor = true;
            this.btnCalcul.Click += new System.EventHandler(this.btnCalcul_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "poids idéal :";
            // 
            // txtPoidsIdeal
            // 
            this.txtPoidsIdeal.Enabled = false;
            this.txtPoidsIdeal.Location = new System.Drawing.Point(90, 133);
            this.txtPoidsIdeal.Name = "txtPoidsIdeal";
            this.txtPoidsIdeal.Size = new System.Drawing.Size(116, 20);
            this.txtPoidsIdeal.TabIndex = 8;
            this.txtPoidsIdeal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 160);
            this.Controls.Add(this.txtPoidsIdeal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCalcul);
            this.Controls.Add(this.dtpNaiss);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTaille);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "CREFF";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTaille;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbLarge;
        private System.Windows.Forms.RadioButton rdbNormale;
        private System.Windows.Forms.RadioButton rdbGracile;
        private System.Windows.Forms.DateTimePicker dtpNaiss;
        private System.Windows.Forms.Button btnCalcul;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPoidsIdeal;
    }
}

