﻿/**
 * Exercice 60 : Calcul du poids idéal suivant la formul de CREFF
 * author : Emds
 * date : 11/07/2017
 */
using System;
using System.Windows.Forms;

namespace Exercice60
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /**
         * Evénement clic sur btnCalcul
         * Calcule le poids idéal en fonction des données saisies
         */
        private void btnCalcul_Click(object sender, EventArgs e)
        {
            // Contrôle si la taille a été saisie et si c'est bien un entier
            try
            {
                // récupération de la taille
                int taille = int.Parse(txtTaille.Text);

                // Calcule de l'âge en fonction de la date de naissance
                int nbJours = (DateTime.Today - dtpNaiss.Value.Date).Days;
                int age = nbJours / 365;

                // Calcule le poids idéal
                float poids = (taille - 100 + (age / 10)) * 0.9f;

                // prise en compte de la morphologie
                if(rdbGracile.Checked)
                {
                    poids *= 0.9f;
                }
                else
                {
                    if (rdbLarge.Checked)
                    {
                        poids *= 1.1f;
                    }
                }

                // affichage du poids idéal
                txtPoidsIdeal.Text = poids.ToString();

            }catch(Exception ex)
            {
                txtTaille.Text = "";
            }

        }

        /**
         * Evénement chargement de la fenêtre
         * Initialisation de composants
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            dtpNaiss.MaxDate = DateTime.Today;
        }

        /**
         * Evénement changement dans txtTaille
         * Vide le poids idéal
         */
        private void txtTaille_TextChanged(object sender, EventArgs e)
        {
            txtPoidsIdeal.Text = "";
        }

        /**
         * Evénement changement de la valeur du bouton radio rdbGracile
         * Vide le poids idéal
         */
        private void rdbGracile_CheckedChanged(object sender, EventArgs e)
        {
            txtPoidsIdeal.Text = "";
        }

        /**
         * Evénement changement de la valeur du bouton radio rdbNormale
         * Vide le poids idéal
         */
        private void rdbNormale_CheckedChanged(object sender, EventArgs e)
        {
            txtPoidsIdeal.Text = "";
        }

        /**
         * Evénement changement de la valeur du bouton radio rdbLarge
         * Vide le poids idéal
         */
        private void rdbLarge_CheckedChanged(object sender, EventArgs e)
        {
            txtPoidsIdeal.Text = "";
        }

        /**
         * Evénement changement de la valeur de la date de naissance
         * Vide le poids idéal
         */
        private void dtpNaiss_ValueChanged(object sender, EventArgs e)
        {
            txtPoidsIdeal.Text = "";
        }
    }
}
