﻿/**
 * Exercice 31 : tableau de distances à 2 dimensions, calcul du déterminant d'une matrice
 * author : Emds
 * date : 03/06/2017
 */
using System;

namespace Exercice31
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclaration
            int[,] matrice = new int[2, 2];

            // saisie de la matrice
            for(int i = 1; i <= 2; i++)
            {
                for(int j = 1; j <= 2; j++)
                {
                    Console.Write("Entrez la valeur de la ligne " + i + ", colonne " + j + " = ");
                    matrice[i-1, j-1] = int.Parse(Console.ReadLine());
                }
            }

            // calcul du déterminant
            int determinant = matrice[0, 0] * matrice[1, 1] - matrice[0, 1] * matrice[1, 0];

            // affiche le déterminant
            Console.WriteLine("déterminant = " + determinant);
            Console.ReadLine();
        }
    }
}
