﻿/**
 * Jeu du nombre caché
 * author : Emds
 * date : 22/05/2017
 */
using System;

namespace NombreCache
{
    class Program
    {
        static void Main(string[] args)
        {
            // déclarations
            int valeur, essai, nbre = 1;

            // saisie du nombre à chercher
            Console.Write("Entrez le nombre à chercher = ");
            valeur = int.Parse(Console.ReadLine());
            Console.Clear();

            // saisie du premier essai
            Console.Write("Entrez un essai = ");
            essai = int.Parse(Console.ReadLine());

            // boucle sur les essais
            while (essai != valeur)
            {
                // test de l'essai par rapport à la valeur à chercher
                if (essai > valeur)
                {
                    Console.WriteLine(" --> trop grand !");
                }
                else
                {
                    Console.WriteLine(" --> trop petit !");
                }

                // saisie d'un essai
                Console.Write("Entrez un essai = ");
                essai = int.Parse(Console.ReadLine());
                
                // compteur d'essais
                nbre++;
            }

            // valeur trouvée et affichage du nombre de tentative
            Console.WriteLine("Vous avez trouvé en "+nbre+" fois !");
            Console.ReadLine();
        }
    }
}
