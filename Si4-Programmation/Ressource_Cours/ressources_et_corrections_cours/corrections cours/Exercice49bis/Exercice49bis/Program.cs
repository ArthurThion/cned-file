﻿/**
 * Exercice 49 bis : calcul d'une distance avec une fonction
 * author : Emds
 * date : 16/06/2017
 */
using System;

namespace Exercice49bis
{
    class Program
    {
        /**
         * Calcul de la distance entre 2 nombres
         */
        static int distance(int val1, int val2)
        {
            if (val1 > val2)
            {
                return (val1 - val2);
            }
            else
            {
                return (val2 - val1);
            }
        }

        static void Main(string[] args)
        {
            // saisie des 2 nombres
            Console.Write("Entrez le premier nombre entier = ");
            int nb1 = int.Parse(Console.ReadLine());
            Console.Write("Entrez le second nombre entier = ");
            int nb2 = int.Parse(Console.ReadLine());

            // affichage de la distance
            Console.WriteLine("Distance entre " + nb1 + " et " + nb2 + " = " + distance(nb1, nb2));
            Console.ReadLine();
        }
    }
}
