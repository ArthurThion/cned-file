﻿/**
 * Exercice 21 : feu tricolore version "if" imbriqués
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice21
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie contrôlée du feu
            char feu;
            do
            {
                Console.WriteLine();
                Console.Write("feu (R/O/V) = ");
                feu = Console.ReadKey().KeyChar;
            } while (feu!='R' && feu!='O' && feu!='V');

            // Affichage de l'ordre
            Console.WriteLine();
            if (feu == 'R')
            {
                Console.WriteLine("s'arrêter");
            }
            else
            {
                if (feu == 'O')
                {
                    Console.WriteLine("ralentir");
                }
                else
                {
                    Console.WriteLine("passer");
                }
            }
            Console.ReadLine();
        }
    }
}
