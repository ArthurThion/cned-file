﻿/**
 * Exercice 15 : remplacement des espaces dans une phrase
 * author : Emds
 * date : 28/05/2017
 */
using System;

namespace Exercice15
{
    class Program
    {
        static void Main(string[] args)
        {
            // Saisie de la phrase
            Console.WriteLine("Entrez une phrase : ");
            string phrase = Console.ReadLine();

            // Remplacement des espaces par _
            string newphrase = phrase.Replace(' ', '_');

            // Affichage de la nouvelle phrase
            Console.WriteLine(newphrase);
            Console.ReadLine();
        }
    }
}
