﻿/**
 * Exercice 53 : procédure de suppression dans un vecteur
 * author : Emds
 * date : 18/06/2017
 */
using System;
using System.Collections.Generic;

namespace Exercice53
{
    class Program
    {
        /**
         * Suppression d'une case dans un vecteur (contenant une valeur précise)
         */
        static void supprVec(string[] vec, string valeur)
        {
            // parcours du vecteur pour trouver la valeur
            int k = 0;
            while (k < vec.Length - 1 && vec[k] != valeur)
            {
                k++;
            }

            // contrôle la sortie (la valeur a-t-elle été trouvée ?)
            if(vec[k] == valeur)
            {
                // suppression de la case en décalant les suivantes
                    for(int j = k ; j < vec.Length - 1 ; j++)
                {
                    vec[j] = vec[j + 1];
                }
                vec[vec.Length - 1] = "";
            }
        }

        static void Main(string[] args)
        {
            // Saisie du nombre de villes
            Console.Write("Entrez le nombre de villes = ");
            int nb = int.Parse(Console.ReadLine());

            // déclarations
            string[] lesVilles = new string[nb];

            // saisie des villes dans un vecteur 
            for (int k = 0; k < nb; k++)
            {
                Console.Write("Entrez la ville n°" + (k + 1) + " = ");
                lesVilles[k] = Console.ReadLine();
            }

            // saisie dela ville à supprimer
            Console.Write("Ville qui doit être supprimée = ");
            string ville = Console.ReadLine();

            // tentative de suppression de la ville dans le vecteur
            supprVec(lesVilles, ville);

            // recalcule la taille (suivant si la dernière case est vide ou non
            int newTaille = nb;
            if(lesVilles[lesVilles.Length-1] == "")
            {
                newTaille = nb - 1;
            }

            // affichage du vecteur final
            for (int k = 0; k < newTaille; k++)
            {
                Console.WriteLine("ville n°" + (k + 1) + " = " + lesVilles[k]);
            }
            Console.ReadLine();
        }
    }
}
