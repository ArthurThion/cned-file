﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercice65
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class Personnage
        {
            private string login;
            private int vie;
            /**
             * Constructeur
             * Valorise le login et initialise la vie à 10
             */
            public Personnage(string login) {}
            /**
             * Permet de faire avancer le joueur
             * retourne faux s'il a été bloqué par un mur
             * et n'a pu avancer jusqu'au bout
             */
            public bool avancer(int distance) { return true; }
            /**
             * Permet de faire pivoter le personnage d'un quart de tour
             * vers la gauche si direction = 'G'
             * vers la droite si direction = 'D'
             */
            public void tourner(char direction) {}
        }

        /**
         * clic sur btnCreer
         * crée le personnage et le fait avancer
         */
        private void btnCreer_Click(object sender, EventArgs e)
        {
            //création du personnage
            Personnage perso = new Personnage("Emds");

            // déclaration pour tourner
            bool gauche = true;

            // boucle pour faire avancer le personnage
            int k = 0;
            while (k < 100)
            {
                // tente d'avancer
                if (perso.avancer(1))
                {
                    // le personnage a pu avancer
                    k++;
                }
                else
                {
                    // bloqué par un mur
                    if (gauche)
                    {
                        perso.tourner('G');
                    }
                    else
                    {
                        perso.tourner('D');
                    }
                    // changer de direction
                    gauche = !gauche;
                }
            }
        }
    }
}
