﻿/**
 * Opérations en mode graphique
 * author : Emds
 * date : 02/07/2017
 */
using System;
using System.Windows.Forms;

namespace OperationsWindows
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /**
         * Evénement Clic sur le bouton btnEffacer (gomme)
         * Mise à blanc des 3 zones de texte
         */
        private void btnEffacer_Click(object sender, EventArgs e)
        {
            txtValeur1.Text = "";
            txtValeur2.Text = "";
            txtResultat.Text = "";
        }

        /**
         * Evénement Clic sur le bouton btnMult (X)
         * Multiplication des 2 premières valeurs et résultat rangé dans la zone du bas
         */
        private void btnMult_Click(object sender, EventArgs e)
        {
            try
            {
                txtResultat.Text = (float.Parse(txtValeur1.Text) * float.Parse(txtValeur2.Text)).ToString();
                lblOperation.Text = "x";
            }
            catch (Exception ex) { };
        }

        /**
         * Evénement Clic sur le bouton btnQuitter
         * Pour quitter l'application
         */
        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /**
         * Evénement Clic sur le bouton btnAdd (+)
         * Addition des 2 premières valeurs et résultat rangé dans la zone du bas
         */
        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            if (txtValeur1.Text != "" && txtValeur2.Text != "")
            {
                try
                {
                    txtResultat.Text = (float.Parse(txtValeur1.Text) + float.Parse(txtValeur2.Text)).ToString();
                    lblOperation.Text = "+";
                }
                catch (Exception ex) { };
            }
        }

        /**
         * Evénement Changement de valeur sur la zone de texte txtValeur1
         * Vide le résultat puisqu'une valeur est modifiée
         */
        private void txtValeur1_TextChanged(object sender, EventArgs e)
        {
            txtResultat.Text = "";
        }

        /**
         * Evénement Changement de valeur sur la zone de texte txtValeur2
         * Vide le résultat puisqu'une valeur est modifiée
         */
        private void txtValeur2_TextChanged(object sender, EventArgs e)
        {
            txtResultat.Text = "";
        }
    }
}
