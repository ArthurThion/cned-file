﻿/**
 * Exercice 43 : reprise de l'exercice 42 avec une fonction
 * author : Emds
 * date : 13/06/2017
 */
using System;

namespace Exercice43
{
    class Program
    {
        /**
         * saisie d'une reponse O ou N
         */
        static char saisie()
        {
            char reponse;
            do
            {
                Console.WriteLine();
                Console.Write("Avez-vous un prix à saisir ? (O/N) ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != 'O' && reponse != 'N');
            return reponse;
        }

        static void Main(string[] args)
        {
            // Déclarations
            float prix, total = 0;
            char rep;

            // demande si un prix est à saisir
            rep = saisie();

            // Boucle sur la saisie des prix et le cumul
            while (rep == 'O')
            {
                // saisie d'un nouveau prix
                Console.Write("   Entrez un prix = ");
                prix = float.Parse(Console.ReadLine());
                // cumul
                total = total + prix;
                // demande si un nouveau prix est à saisir
                rep = saisie();
            }

            // Affichage du total
            Console.WriteLine("   total des prix = " + total);
            Console.ReadLine();
        }
    }
}
