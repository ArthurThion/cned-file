﻿/**
 * Exercice 10 : saisie du sexe avec contrôle de saisie
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice10
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclaration
            char sexe;

            // Boucle sur la saisie correcte du sexe
            do
            {
                Console.WriteLine();
                Console.Write("Quel est votre sexe ? (H/F) ");
                sexe = Console.ReadKey().KeyChar;
            } while (sexe != 'H' && sexe != 'F');

            // Affichage du message personnalisé
            Console.WriteLine();
            if (sexe == 'H')
            {
                Console.WriteLine("Bonjour monsieur");
            }
            else
            {
                Console.WriteLine("Bonjour madame");
            }
            Console.ReadLine();
        }
    }
}
