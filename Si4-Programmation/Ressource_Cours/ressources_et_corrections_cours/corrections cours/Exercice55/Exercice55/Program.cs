﻿/**
 * Exercice 55 : notation polonaise
 * author : Emds
 * date : 23/06/2017
 */
using System;

namespace Exercice55
{
    class Program
    {
        /**
         * saisie d'une reponse d'un caractère parmi 2
         */
        static char saisie(string message, char carac1, char carac2)
        {
            char reponse;
            do
            {
                Console.WriteLine();
                Console.Write(message + " (" + carac1 + "/" + carac2 + ") ");
                reponse = Console.ReadKey().KeyChar;
            } while (reponse != carac1 && reponse != carac2);
            return reponse;
        }

        /**
         * retourne le résultat du calcul d'une formule de notation polonaise
         */
        static float polonaise(String formule)
        {
            // transformation de la formule en vecteur
            string[] vec = formule.Split(' ');
            int nbCases = vec.Length;

            //--- boucle tantque qu'il ne reste pas qu'une seule case ---
            while (nbCases > 1)
            {
                // recherche d'un signe à partir de la fin
                int k = nbCases - 1;
                while (k > 0 && vec[k] != "+" && vec[k] != "-" && vec[k] != "*" && vec[k] != "/")
                {
                    k--;
                }

                // récupération des 2 valeurs concernées par le calcul
                float a = float.Parse(vec[k + 1]);
                float b = float.Parse(vec[k + 2]);

                // calcul
                float result = 0;
                switch (vec[k])
                {
                    case "+" : result = a + b; break; 
                    case "-" : result = a - b; break; 
                    case "*" : result = a * b; break; 
                    case "/" : result = a / b; break; 
                }

                // stockage du résultat
                vec[k] = result.ToString();

                // modification du vecteur (supprimer les 2 cellules en trop)
                for(int j=k+1; j<nbCases-2;j++)
                {
                    vec[j] = vec[j + 2];
                }
                nbCases = nbCases - 2;
            }

            // retour du résultat final
            return float.Parse(vec[0]);
        }

        static void Main(string[] args)
        {
            // déclarations
            char reponse;

            // boucle sur la saisie de la formule
            do
            {
                // saisie de la formule
                Console.WriteLine();
                Console.WriteLine("entrez une formule polonaise en séparant chaque partie par un espace = ");
                string laFormule = Console.ReadLine();

                // affichage du résultat
                Console.WriteLine("Résultat =  " + polonaise(laFormule));

                // demande s'il faut continuer
                reponse = saisie("Voulez-vous continuer ? (O/N) ", 'O', 'N');

            } while (reponse == 'O');
        }
    }
}
