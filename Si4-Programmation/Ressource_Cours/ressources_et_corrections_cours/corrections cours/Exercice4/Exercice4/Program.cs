﻿/**
 * Exercice 4 : calcul d'un prix TTC
 * author : Emds
 * date : 25/05/2017
 */
using System;

namespace Exercice4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclarations
            float prixht, tva;

            // Saisie du prix HT et de la TVA
            Console.Write("Entrez le prix HT (ex : 25,43) = ");
            prixht = float.Parse(Console.ReadLine());
            Console.Write("Entrez le taux de TVA (ex : 19,6) = ");
            tva = float.Parse(Console.ReadLine());

            // Affichage du prix TTC
            Console.WriteLine("prix TTC = " + (prixht * (1 + (tva / 100))));
            Console.ReadLine();
        }
    }
}
