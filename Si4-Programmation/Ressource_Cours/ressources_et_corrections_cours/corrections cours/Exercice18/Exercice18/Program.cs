﻿/**
 * Exercice 18 : min de 3 nombres sans faire de test
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice18
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie des 3 nombres
            Console.Write("Entrez le nombre 1 = ");
            double nb1 = double.Parse(Console.ReadLine());
            Console.Write("Entrez le nombre 2 = ");
            double nb2 = double.Parse(Console.ReadLine());
            Console.Write("Entrez le nombre 3 = ");
            double nb3 = double.Parse(Console.ReadLine());

            // affichage du plus petit des 3 nombres
            Console.WriteLine("Le plus petit est " + Math.Min(nb1, Math.Min(nb2, nb3)));
            Console.ReadLine();
        }
    }
}
