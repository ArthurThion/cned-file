﻿/**
 * Exercice 8 : itération avec saisie de prix en posant la question, puis affichage du total
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice8
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclarations
            float prix, total = 0;
            char reponse;

            // demande si un prix est à saisir
            Console.Write("Avez-vous un prix à saisir ? (O/N) ");
            reponse = Console.ReadKey().KeyChar;

            // Boucle sur la saisie des prix et le cumul
            while (reponse=='O')
            {
                // saisie d'un nouveau prix
                Console.Write("   Entrez un prix = ");
                prix = float.Parse(Console.ReadLine());
                // cumul
                total = total + prix;
                // demande si un nouveau prix est à saisir
                Console.Write("Avez-vous un prix à saisir ? (O/N) ");
                reponse = Console.ReadKey().KeyChar;
            }

            // Affichage du total
            Console.WriteLine("   total des prix = " + total);
            Console.ReadLine();
        }
    }
}
