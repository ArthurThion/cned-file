﻿/**
 * Exercice 9 : contrôle de saisie d'une note
 * author : Emds
 * date : 27/05/2017
 */
using System;

namespace Exercice9
{
    class Program
    {
        static void Main(string[] args)
        {
            // Déclarations
            int note;

            // Boucle sur le contrôle de la saisie de la note
            do
            {
                Console.Write("Entrez une note entre 0 et 20 = ");
                note = int.Parse(Console.ReadLine());
            } while (note < 0 || note > 20);

            // Affichage de la note
            Console.WriteLine("note saisie : " + note);
            Console.ReadLine();
        }
    }
}
