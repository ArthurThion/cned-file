﻿/**
 * Exercice 34 : tri par insertion
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice34
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie du nombre de cases
            Console.Write("Nombre de cases du vecteur = ");
            int nb = int.Parse(Console.ReadLine());

            // déclaration du vecteur
            int[] vec = new int[nb];

            // saisie du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write("saisir le nombre n°" + (k + 1) + " = ");
                vec[k] = int.Parse(Console.ReadLine());
            }

            // affichage de la première version du vecteur
            for (int k = 0; k < nb; k++)
            {
                Console.Write(vec[k] + " ");
            }
            Console.WriteLine();

            // tri par sélection et affichage
            for (int i = 1; i < nb; i++)
            {
                int sauv = vec[i];
                // décalage des valeurs pour positionner la plus petite du sous-vecteur
                int j = i - 1;
                while(j >= 0 && sauv < vec[j])
                {
                    vec[j + 1] = vec[j];
                    j--;
                }
                // insertion de la valeur la plus petite du sous-vecteur
                vec[j + 1] = sauv;
                // affichage de la nouvelle version du vecteur
                for (int k = 0; k < nb; k++)
                {
                    Console.Write(vec[k] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
