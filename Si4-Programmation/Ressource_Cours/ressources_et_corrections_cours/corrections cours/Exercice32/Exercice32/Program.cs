﻿/**
 * Exercice 32 : tableau de structure, nom et moyenne d'étudiants
 * author : Emds
 * date : 04/06/2017
 */
using System;

namespace Exercice32
{

    class Program
    {

        // structure
        struct StrEtudiant
        {
            public string nom;
            public float moy;
        }

        static void Main(string[] args)
        {
            // saisie du nombre d'étudiants
            Console.Write("Nombre d'étudiants = ");
            int nb = int.Parse(Console.ReadLine());

            // déclarations
            StrEtudiant[] etudiant = new StrEtudiant[nb];
            float moyenne = 0;

            // saisie des noms er moyennes, et cumul des moyennes
            for(int k = 0; k < nb; k++)
            {
                Console.Write("Etudiant n° " + (k+1) + " nom = ");
                etudiant[k].nom = Console.ReadLine();
                Console.Write("Etudiant n° " + (k+1) + " moyenne = ");
                etudiant[k].moy = float.Parse(Console.ReadLine());
                moyenne += etudiant[k].moy;
            }

            // calcul de la moyenne
            moyenne = moyenne / nb;

            // affichage des noms des étudiants au dessus de la moyenne de la classe
            Console.WriteLine("Etudiants au dessus de la moyenne de la classe :");
            for (int k = 0; k < nb; k++)
            {
                if(etudiant[k].moy >= moyenne)
                {
                    Console.Write(etudiant[k].nom + " ");
                }
            }
            Console.ReadLine();
        }
    }
}
