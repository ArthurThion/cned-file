﻿/**
 * Jeu du nombre caché avec fontion
 * author : Emds
 * date : 11/06/2017
 */
using System;

namespace NombreCacheFonction
{
    class Program
    {

        /**
         * saisie d'un nombre entre 2 bornes
         */
        static int saisie(string message, int inf, int sup)
        {
            int nombre;
            do
            {
                Console.Write(message + " (entre "+inf+" et "+sup+") = ");
                nombre = int.Parse(Console.ReadLine());
            } while (nombre < inf || nombre > sup);
            return nombre;
        }

        static void Main(string[] args)
        {
            // déclarations
            int valeur, essai, nbre = 1, borneInf = 0, borneSup = 100;

            // saisie du nombre à chercher
            valeur = saisie("Entrez le nombre à chercher", borneInf, borneSup);
            Console.Clear();

            // saisie du premier essai
            essai = saisie("Entrez un essai", borneInf, borneSup);

            // boucle sur les essais
            while (essai != valeur)
            {
                // test de l'essai par rapport à la valeur à chercher
                if (essai > valeur)
                {
                    Console.WriteLine(" --> trop grand !");
                }
                else
                {
                    Console.WriteLine(" --> trop petit !");
                }

                // saisie d'un essai
                essai = saisie("Entrez un essai", borneInf, borneSup);

                // compteur d'essais
                nbre++;
            }

            // valeur trouvée et affichage du nombre de tentative
            Console.WriteLine("Vous avez trouvé en " + nbre + " fois !");
            Console.ReadLine();
        }
    }
}
