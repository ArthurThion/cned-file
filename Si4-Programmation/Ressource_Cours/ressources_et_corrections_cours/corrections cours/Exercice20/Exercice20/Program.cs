﻿/**
 * Exercice 20 : affichage d'une ville et du nombre d'habitants
 * author : Emds
 * date : 29/05/2017
 */
using System;

namespace Exercice20
{
    class Program
    {
        static void Main(string[] args)
        {
            // saisie de la ville et du nombre d'habitants
            Console.Write("Entrez la ville = ");
            string ville = Console.ReadLine();
            Console.Write("Entrez le nobmre d'habitants = ");
            int habitants = int.Parse(Console.ReadLine());

            // affichage du message
            Console.WriteLine(ville + " possède " + habitants + " habitants.");
            Console.ReadLine();
        }
    }
}
