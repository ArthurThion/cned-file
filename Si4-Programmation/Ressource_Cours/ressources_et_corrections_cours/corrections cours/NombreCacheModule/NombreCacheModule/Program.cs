﻿/**
 * Jeu du nombre caché avec module simple
 * author : Emds
 * date : 05/06/2017
 */
using System;

namespace NombreCacheModule
{
    class Program
    {
        // déclarations globales
        static int essai;

        /**
         * saisie de l'essai entre 0 et 100
         */
        static void saisie()
        {
            do
            {
                Console.Write("Entrez un essai (entre 0 et 100) = ");
                essai = int.Parse(Console.ReadLine());
            } while (essai < 0 || essai > 100);
        }

        static void Main(string[] args)
        {
            // déclarations
            int valeur, nbre = 1;

            // saisie du nombre à chercher
            Console.Write("Entrez le nombre à chercher = ");
            valeur = int.Parse(Console.ReadLine());
            Console.Clear();

            // saisie du premier essai
            saisie();

            // boucle sur les essais
            while (essai != valeur)
            {
                // test de l'essai par rapport à la valeur à chercher
                if (essai > valeur)
                {
                    Console.WriteLine(" --> trop grand !");
                }
                else
                {
                    Console.WriteLine(" --> trop petit !");
                }

                // saisie d'un essai
                saisie();

                // compteur d'essais
                nbre++;
            }

            // valeur trouvée et affichage du nombre de tentative
            Console.WriteLine("Vous avez trouvé en " + nbre + " fois !");
            Console.ReadLine();
        }
    }
}
