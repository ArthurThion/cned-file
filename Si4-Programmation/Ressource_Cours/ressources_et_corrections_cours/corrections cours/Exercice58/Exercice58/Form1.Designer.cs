﻿namespace Exercice58
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBase10 = new System.Windows.Forms.GroupBox();
            this.txtBase10 = new System.Windows.Forms.TextBox();
            this.grpBase2 = new System.Windows.Forms.GroupBox();
            this.txtBase2 = new System.Windows.Forms.TextBox();
            this.grpBase10.SuspendLayout();
            this.grpBase2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBase10
            // 
            this.grpBase10.Controls.Add(this.txtBase10);
            this.grpBase10.Location = new System.Drawing.Point(12, 12);
            this.grpBase10.Name = "grpBase10";
            this.grpBase10.Size = new System.Drawing.Size(220, 46);
            this.grpBase10.TabIndex = 0;
            this.grpBase10.TabStop = false;
            this.grpBase10.Text = "base 10";
            // 
            // txtBase10
            // 
            this.txtBase10.Location = new System.Drawing.Point(6, 19);
            this.txtBase10.Name = "txtBase10";
            this.txtBase10.Size = new System.Drawing.Size(208, 20);
            this.txtBase10.TabIndex = 0;
            this.txtBase10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBase10.TextChanged += new System.EventHandler(this.txtBase10_TextChanged);
            // 
            // grpBase2
            // 
            this.grpBase2.Controls.Add(this.txtBase2);
            this.grpBase2.Location = new System.Drawing.Point(12, 64);
            this.grpBase2.Name = "grpBase2";
            this.grpBase2.Size = new System.Drawing.Size(220, 47);
            this.grpBase2.TabIndex = 1;
            this.grpBase2.TabStop = false;
            this.grpBase2.Text = "base 2";
            // 
            // txtBase2
            // 
            this.txtBase2.Location = new System.Drawing.Point(6, 19);
            this.txtBase2.Name = "txtBase2";
            this.txtBase2.Size = new System.Drawing.Size(208, 20);
            this.txtBase2.TabIndex = 0;
            this.txtBase2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBase2.TextChanged += new System.EventHandler(this.txtBase2_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 121);
            this.Controls.Add(this.grpBase2);
            this.Controls.Add(this.grpBase10);
            this.Name = "Form1";
            this.Text = "Convertisseur";
            this.grpBase10.ResumeLayout(false);
            this.grpBase10.PerformLayout();
            this.grpBase2.ResumeLayout(false);
            this.grpBase2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBase10;
        private System.Windows.Forms.TextBox txtBase10;
        private System.Windows.Forms.GroupBox grpBase2;
        private System.Windows.Forms.TextBox txtBase2;
    }
}

