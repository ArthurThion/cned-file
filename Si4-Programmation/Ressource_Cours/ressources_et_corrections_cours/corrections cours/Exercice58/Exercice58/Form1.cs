﻿/**
 * Exercice 58 : convertisseur base10 base2
 * author : Emds
 * date : 07/07/2017
 */
using System;
using System.Windows.Forms;

namespace Exercice58
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /**
         * Conversion d'un entier base 10 vers un nombre en base 2 (binaire)
         */
        private string conversionBase10ToBase2(int base10)
        {
            // Conversion par divisions successives
            string base2 = "";
            while (base10 != 0)
            {
                base2 = (base10 % 2) + base2;
                base10 = base10 / 2;
            }
            // cas particulier de base2 vide
            if (base2 == "")
            {
                base2 = "0";
            }
            return base2;
        }

        /**
         * Conversion d'un nombre base 2 (binaire) vers un nombre en base 10
         * retourne -1 en cas d'erreur
         */
        private int conversionBase2ToBase10(string base2)
        {
            // contrôle que le contenu est bien binaire
            if (isBinaire(base2))
            {
                // Conversion par multiplications successives par puissance de 2
                int base10 = 0;
                for (int k = 0; k < base2.Length ; k++)
                {
                    base10 += int.Parse(base2.Substring(base2.Length-k-1, 1)) * (int)Math.Pow(2, k);
                }
                return base10;
            }
            else
            {
                // conversion impossible
                return -1;
            }
        }

        /**
         * Changement de valeur dans la zone de saisie txtBase10
         * Conversion directe en binaire
         */
        private void txtBase10_TextChanged(object sender, EventArgs e)
        {
            // contrôle que la conversion fonctionne
            try
            {
                int base10 = int.Parse(txtBase10.Text);
                // affichage de la conversiob en base 2
                txtBase2.Text = conversionBase10ToBase2(base10);
            }catch(Exception ex)
            {
                // erreur, le contenu de la zone de texte n'est pas un entier
                txtBase10.Text = "";
                txtBase2.Text = "";
            }
        }

        /**
         * Changement de valeur dans la zone de saisie txtBase2
         * Conversion directe en décimal
         */
        private void txtBase2_TextChanged(object sender, EventArgs e)
        {
            // Récupération du contenu converti en base 10
            int base10 = conversionBase2ToBase10(txtBase2.Text);
            // Vérifie si la conversion a marché
            if (base10 != -1)
            {
                txtBase10.Text = base10.ToString();
            }
            else
            {
                // erreur de conversion, la zone de texte ne contient pas un binaire
                txtBase10.Text = "";
                txtBase2.Text = "";
            }
        }

        /**
         * Conrtôle si le contenu d'une chaine est un nombre binaire
         */
        private bool isBinaire(string texte)
        {
            // contrôle que le texte ne contient que des 1 et des 0
            int k = 0;
            while (k < texte.Length && 
                (texte.Substring(k, 1) == "0" || texte.Substring(k, 1) == "1"))
            {
                k++;
            }
            // retourne vrai si tous les caractères sont des "0" ou "1"
            return (k == texte.Length);
        }
    }
}
