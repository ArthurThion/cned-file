package controleur;

/**
 * Regroupement des constantes de l'application
 * @author emds
 *
 */
public interface Global {
	
	public static final Integer PORT = 6666 ;
	
	// fichiers
	public static final String
		SEPARATOR = "//",
		CHEMIN = "media" + SEPARATOR,
		CHEMINFONDS = CHEMIN + "fonds" + SEPARATOR,
		CHEMINPERSOS = CHEMIN + "personnages" + SEPARATOR,
		CHEMINMURS = CHEMIN + "murs" + SEPARATOR,
		CHEMINBOULES = CHEMIN + "boules" + SEPARATOR,
		EXTIMAGE = ".gif" ;

	// images
	public static final String
	FONDCHOIX = CHEMINFONDS + "fondchoix.jpg",	
	FONDARENE = CHEMINFONDS+"fondarene.jpg",
	PERSO = CHEMINPERSOS + "perso",
	BOULE = CHEMINBOULES + "boule.gif",
	MUR = CHEMINMURS + "mur.gif" ;
	
	// personnages
	public static final int
		GAUCHE = 0,
		DROITE = 1,
		HAUT = 2,
		BAS = 3,
		TIRE = 4,
		NBETATSMARCHE = 4,
		NBETATSBLESSE = 2,
		NBETATSMORT = 2,
		LEPAS = 10, // nombre de pixels de déplacement d'un pas
		NBPERSOS = 3,
		H_MESSAGE = 8,
		H_PERSO = 44,
		L_PERSO = 39 ;
	public static final String
		MARCHE = "marche",
		BLESSE = "touche",
		MORT = "mort" ;
	
	// messages serveurs
	public static final String SEPARE = "~" ;
	public static final int
		PSEUDO = 0,
		CHAT = 1,
		ACTION = 2 ;
	
	// tailles dans frames
	public static final int
		H_ARENE = 600,
		L_ARENE = 800,
		H_CHAT = 200,
		H_SAISIE = 25,
		MARGE = 5 ; // écarts entre les objets
	
	// murs
	public static final int
		NBMURS = 20, // nombre de murs
		H_MUR = 35, // hauteur du mur
		L_MUR = 34 ; // largeur du mur
	
	// boule
	public static final int
	H_BOULE = 17, // hauteur de la boule
	L_BOULE = 17 ; // largeur de la boule
	
}
