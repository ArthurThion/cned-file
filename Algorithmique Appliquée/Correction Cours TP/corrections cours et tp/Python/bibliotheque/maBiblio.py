# -*-coding:utf-8 -*
# Bibliotheque maBiblio
# contient la fonction premier
# date : 16/05/2013
# auteur : Emds

import os
import math


#------------------------------------------------------------------------------------
# fonction premier
# but : contrôle si un entier naturel est premier
# entrée : unEntier (entier naturel) contient le nombre à contrôler
# sortie : booléen (vrai si le nombre est premier)
def premier(unEntier) :
    """ contrôle si un entier naturel est premier """
    # boucle sur la recherche d'un diviseur
    diviseur = 2
    p = True # mémorise si le nombre est premier ou non
    while p and diviseur*diviseur<=unEntier :
        if unEntier % diviseur == 0 :
            p = False
        diviseur += 1
    return p 

#------------------------------------------------------------------------------------
# procedure multiples
# but : affiche les multiples d'un nombre
# entrée : unNombre (entier naturel) le nombre dont les multiples doivent être affichés
#          nbMultiples (entier naturel) nombre de multiples à afficher
def multiples(unNombre, nbMultiples) :
    """ affiche les multiples d'un nombre """
    for k in range(1, nbMultiples+1) :
        print(unNombre*k)

#------------------------------------------------------------------------------------
# procedure facteurPremiers
# but : affiche les facteurs premiers d'un nombre
# entrée : unNombre (entier naturel) le nombre dont les facteurs premiers doivent être affichés
def facteursPremiers(unNombre) :
    """ affiche les facteurs premiers d'un nombre """
    # recherche des facteurs premiers
    diviseur = 2
    while diviseur<=unNombre :
        # si on trouve un facteur premier
        if unNombre%diviseur == 0 :
            # on l'affiche
            print(diviseur)
            # on divise la valeur pour éviter les facteurs non premiers
            unNombre = unNombre // diviseur
        else :
            diviseur += 1

#------------------------------------------------------------------------------------
# procedure nombresPremiers
# but : affiche les premiers nombres premiers
# entrée : nbPremiers (entier naturel) le nombre de nombres premiers à afficher
def nombresPremiers(nbNombre) :
    """ affiche les premiers nombres premiers """
    # déclarations
    p = [0]*1000 # tableau qui mémorisera les nombres premiers
    max = 1 # max contiendra le nombre de cases remplies dans premier
    nb = 3 # variable qui sera à tester

    # limitation de la taille à 1000 nombres
    if nbNombre > 1000 :
        nbNombre = 1000

    # initialisation de la première case
    p[0] = 2

    # boucle sur la recherche des premiers
    while max<nbNombre :
        # test si le nombre contenu dans nb est premier
        k = 0 
        while k<max and math.pow(p[k], 2)<nb and nb%p[k]!=0 :
            k += 1
        # si nb est premier, il est mémorisé
        if nb%p[k]!=0 :
            p[max] = nb
            max += 1
        # passage au nombre suivant à tester
        nb += 1

    # affichage des 50 nombres premiers
    for k in range(0, nbNombre) :
        print(p[k])

#------------------------------------------------------------------------------------
# fonction factorielle
# but : calcul de la factorielle d'un nombre
# entrée : unEntier (entier naturel) le nombre dont la factorielle doit être calculée
# sortie : entier contenant la factorielle
def factorielle(unEntier) :
    """ calcul de la factorielle d'un nombre """
    if unEntier==1 :
        return 1
    else :
        return unEntier * factorielle(unEntier - 1)
