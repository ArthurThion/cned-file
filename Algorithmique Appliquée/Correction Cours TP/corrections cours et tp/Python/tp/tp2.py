# -*-coding:utf-8 -*
# Programme matriceInverse tp 2
# saisie d'une matrice carré et inversion de la matrice
# date : 16/05/2013
# auteur : Emds

import os
import sys
sys.path.append("..\\bibliotheque")
import colorama
from colorama.initialise import gotoxy 


N = 3 # constante pour la taille des matrices

# fonction copie d'une matrice de m2 vers m1
def memcpy(m1, m2) :
    for i in range(0, N) :
        for j in range(0, N) :
            m1[i][j] = m2[i][j]
    

# déclarations
mdep = [] # matrice de départ à inverser
mtrans = [] # matrice transitoire pour le calcul
minv = [] # matrice inversée
mid = [] # matrice identité

# initialisation des matrices à 0
for i in range(0, N) :
    mdep.append([])
    mtrans.append([])
    minv.append([])
    mid.append([])
    for j in range(0, N) :
        mdep[i].append(0)
        mtrans[i].append(0)
        minv[i].append(0)
        mid[i].append(0)


# initialisation de la matrice identité (remplissage des 1 en diagonale)
for i in range(0, N) :
    mid[i][i] = 1

# affichage de la structure de la matrice à saisir
for i in range(0, N) :
    for j in range(0, N) :
        gotoxy(i*10+1, j*2+1)
        print(".")

# saisie de la matrice NxN
for i in range(0, N) :
    for j in range(0, N) :
        gotoxy(i*10+1, j*2+1)
        mdep[i][j] = int(input())

#--- calcul de la matrice inverse ---
memcpy(mtrans, mdep) # mtrans <-- mdep
memcpy(minv, mid) # minv <-- mid
# parcours des colonnes
for i in range(0, N) :
    # parcours des lignes de la colonne
    for j in range(0, N) :
        # si on trouve un 0 sur la diagonale de la matrice de départ, inversion impossible
        if mdep[i][i] == 0 :
            print("inversion inmpossible")
            break
        # calculs
        mtrans[i][j] = mdep[i][j] / mdep[i][i] 
        mid[i][j] = minv[i][j] / mdep[i][i] 
    memcpy(mdep, mtrans) # mdep <-- mtrans
    memcpy(minv, mid) # minv <-- mid
    # parcours complet colonnes/lignes excepté la colonne où on se trouve (i)
    for k in range(0, N) :
        if k != i :
            for j in range(0, N) :
                # calculs
                mtrans[k][j] = mdep[k][j] - mdep[i][j] * mdep[k][i] 
                mid[k][j] = minv[k][j] - minv[i][j] * mdep[k][i] 
    memcpy(mdep, mtrans) # mdep <-- mtrans
    memcpy(minv, mid) # minv <-- mid

# affichage de la matrice inverse
for i in range(0, N) :
    for j in range(0, N) :
        gotoxy(i*10+1, j*2+10)
        print(minv[i][j])
    

os.system("pause")
