# -*-coding:utf-8 -*
# Programme matriceInverse tp 3
# calcul des chemins de longueur p et de la fermeture transitive d'un graphe
# date : 16/05/2013
# auteur : Emds

import os
import sys
sys.path.append("..\\bibliotheque")
import colorama
from colorama.initialise import gotoxy 


N = 100 # constante pour la taille des matrices

#---------------------------------------------------------------------------
# fonction initialisation matrice
def initmat(m, val) :
    for i in range(0, N) :
        m.append([])
        for j in range(0, N) :
            m[i].append(val)
    
#---------------------------------------------------------------------------
# Variables globales
matrice = [] # matrice d'adjacence du graphe
initmat(matrice, 0) 
taille = 0 # nombre de neouds du graphe

#---------------------------------------------------------------------------
# fonction copie d'une matrice de m2 vers m1
def memcpy(m1, m2) :
    for i in range(0, N) :
        for j in range(0, N) :
            m1[i][j] = m2[i][j]
    
#---------------------------------------------------------------------------
# fonction saisieEntierControle
# but : saisie d'une valeur entière avec contrôle
# entrée : message (chaine) message à afficher avant la saisie
#          min, max (entier) bornes limites pour la saisie
# sortie : valeur saisie
def saisieEntierControle (message, min, max, x, y) :
    valeur = max+1
    while valeur<min or valeur>max :
        gotoxy(x, y)
        valeur = int(input(message))
    return valeur

#---------------------------------------------------------------------------
# procedure afficheMatrice
# but : affichage d'une matrice
def afficheMatrice (m, taillem, x, y) :
    for j in range(0, taillem) :
        for i in range(0, taillem) :
            gotoxy(x+i*4+2, y+j*2+4)
            print(m[i][j])

#---------------------------------------------------------------------------
# procedure saisieMatrice
# but : saisie d'une matrice
def saisieMatrice() :
    global taille
    clrscr = os.system('cls')
    taille = saisieEntierControle("Nombre de noeuds du graphe = ", 1, 100, 1, 1)
    print("Saisie de la matrice d'adjacence du graphe (0 ou 1) : ")
    # affichage des points
    for j in range(0, taille) :
        for i in range(0, taille) :
            gotoxy(i*4+2, j*2+4)
            print(".")
    # saisie de la matrice
    for j in range(0, taille) :
        for i in range(0, taille) :
            matrice[i][j] = saisieEntierControle("", 0, 1, i*4+2, j*2+4)

#---------------------------------------------------------------------------
# procedure longueurs
# but : calcul de la matrice des chemins de longueur p
def longueurs() :
    global taille
    clrscr = os.system('cls')    
    # contrôle si une matrice a été saisie
    if taille==0 :
        print("Vous devez d'abord saisir une matrice d'adjacence...")
    else :
        # saisie de la longueur
        p = saisieEntierControle("Longueur du chemin = ", 1, taille, 1, 1)
        puissance = []
        initmat(puissance, 0)
        # copie de la matrice d'origine dans une matrice de transition
        transition = []
        initmat(transition, 0)
        memcpy(transition, matrice)
        # calcul de la puissance
        for k in range(1, p) :
            # la matrice est élevée au carré
            for j in range(0, taille) :
                for i in range(0, taille) :
                    puissance[i][j] = 0 
                    for m in range(0, taille) :
                        puissance[i][j] += transition[i][m]*matrice[m][j]
            # transfert de la matrice pour passer à la puissance suivante
            memcpy(transition, puissance) 

        # affichage des 2 matrices
        afficheMatrice(matrice, taille, 1, 1)
        afficheMatrice(puissance, taille, 1, taille*2 + 4)

#---------------------------------------------------------------------------
# procedure fermeture
# but : calcul de la matrice de la fermeture transitive
def fermeture() :
    global taille
    clrscr = os.system('cls')    
    # contrôle si une matrice a été saisie
    if taille==0 :
        print("Vous devez d'abord saisir une matrice d'adjacence...")
    else :
        fermtrans = []
        initmat(fermtrans, 0)
        # copie de la matrice d'origine dans une matrice de fermeture transitive
        memcpy(fermtrans, matrice)
        # algorithme de warshall
        for i in range(0, taille) :
            fermtrans[i][i] = 1 
        for k in range(0, taille) :
            for i in range(0, taille) :
                for j in range(0, taille) :
                    fermtrans[i][j] = fermtrans[i][j] or (fermtrans[i][k] and fermtrans[k][j])

        # affichage des 2 matrices
        afficheMatrice(matrice, taille, 1, 1)
        afficheMatrice(fermtrans, taille, 1, taille*2 + 4)

#----------------------------------------------------------------------
# PROGRAMME PRINCIPAL

# initialisation
choix = 'Z' # mémorisation du choix de l'utilisateur
taille = 0 # nombre de neouds

# boucle sur le menu général
while choix != 'Q' and choix != 'q' :

    # affichage du menu et saisie du choix
    print("Saisie d'une nouvelle matrice ............... 1")
    print("Matrice des chemins de longueur p ........... 2")
    print("Fermeture transitive ........................ 3")
    print("Quitter ..................................... Q")
    choix = input("Choix :                                       ")

    # traitement des différents choix
    if choix=='1' :
        saisieMatrice()
    else :
        if choix=='2' :
            longueurs()
        else :
            if choix=='3' :
                fermeture()

    

os.system("pause")
