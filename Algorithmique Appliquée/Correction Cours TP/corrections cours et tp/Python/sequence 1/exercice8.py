# -*-coding:utf-8 -*
# Programme exercice 8
# opération logique ET entre 2 nombres binaires
# date : 16/05/2013
# auteur : Emds

import os
import math


# saisie du premier nombre binaire
binaire = input("Entrez un premier nombre binaire = ")

# conversion en entier du premier nombre
nb1 = 0 
k = 0 
while len(binaire) > 0 :
    # extration du dernier caractère de binaire
    s = binaire[len(binaire)-1:]
    # conversion du caractère en entier
    result = int(s)
    # ajout dans nb du bit récupéré multiplié par la puissance de 2
    nb1 += int(result * math.pow(2, k))
    #on enlève le dernier caractère
    binaire = binaire[:len(binaire)-1]
    k += 1 
print("conversion en base 10 = ", nb1)

# saisie du second nombre binaire
binaire = input("Entrez un second nombre binaire = ")

# conversion en entier du second nombre
nb2 = 0 
k = 0 
while len(binaire) > 0 :
    # extration du dernier caractère de binaire
    s = binaire[len(binaire)-1:]
    # conversion du caractère en entier
    result = int(s)
    # ajout dans nb du bit récupéré multiplié par la puissance de 2
    nb2 += int(result * math.pow(2, k)) 
    #on enlève le dernier caractère
    binaire = binaire[:len(binaire)-1]
    k += 1 
print("conversion en base 10 = ", nb2)

# calcul du ET entre les 2
resultat = nb1 & nb2 
print("résultat du ET en base 10 = ", resultat)

# conversion du résultat pour l'affichage
binaire = ""
while resultat != 0 :
    # récupération du reste et conversion en caractère
    c = str(resultat % 2)
    # concaténation du caractère
    binaire = c + binaire 
    resultat = resultat // 2 
print("conversion en binaire = ", binaire)


os.system("pause")
