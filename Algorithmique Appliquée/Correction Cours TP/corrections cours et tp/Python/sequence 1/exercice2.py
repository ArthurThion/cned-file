# -*-coding:utf-8 -*
# Programme exercice 2
# afichage d'une table de multiplication avec contrôle de saisie
# date : 16/05/2013
# auteur : Emds

import os


k = 1

# saisie de la valeur avec contrôle de saisie
val = 0
while val < 1 or val > 9 :
    val = int(input("Entrez un nombre entier = "))

# affichage de la table de multiplication pour cette valeur
for k in range(1, 11) :
    print(val, " * ", k, " = ", (val*k), "\n")


os.system("pause")
