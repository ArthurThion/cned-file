# -*-coding:utf-8 -*
# Programme PGCD
# date : 16/05/2013
# auteur : Emds

import os

a = int(input("Entrez le premier nombre = "))
b = int(input("Entrez le second nombre = "))
r = a % b
while r != 0 :
    a = b
    b = r
    r = a % b
print("le PGCD est ", b)

os.system("pause")
