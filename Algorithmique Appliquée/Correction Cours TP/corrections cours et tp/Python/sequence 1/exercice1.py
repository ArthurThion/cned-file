# -*-coding:utf-8 -*
# Programme exercice 1
# afichage d'une table de multiplication
# date : 16/05/2013
# auteur : Emds

import os


k = 1

# saisie de la valeur
val = int(input("Entrez un nombre entier = "))

# affichage de la table de multiplication pour cette valeur
while k <= 10 :
    print(val, " * ", k, " = ", (val*k), "\n")
    k += 1


os.system("pause")
