/* programme test3
*  but : effet de bord avec débordement
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string prenom ;
    cout << "Entre votre pr\x82nom (sans espace) = " ;
    cin >> prenom ;
    for (int k=0 ; k<=prenom.size() ; k++) {
        cout << prenom.substr(k, 1) << "*" ;
    }

    return 0;
}
