/* programme premier (exercice6)
*  but : contr�le si un nombre est premier ou non
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    // d�clarations et initialisations
    int val, diviseur ;
    bool premier ;

    // saisie du premier nombre � tester
    cout << "entrez un nombre entier (0 pour finir) : " ;
    cin >> val ;

    // boucle g�n�rale sur les tests
    while (val != 0)
    {
        // boucle sur la recherche d'un diviseur
        diviseur = 2 ;
        premier = true ;
        while (premier && diviseur*diviseur<=val)
        {
            if (val % diviseur == 0)
            {
                premier = false ;
            }
            diviseur = diviseur + 1 ;
        }

        // affichage du message correspondant au nombre
        if (premier)
        {
            cout << val << " est premier" ;
        }
        else
        {
            cout << val << " n'est pas premier" ;
        }

        // saisie d'un nombre � tester
        cout << endl << "entrez un nombre entier (0 pour finir) : " ;
        cin >> val ;
    }

    return 0;
}
