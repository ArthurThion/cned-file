/* programme test2
*  but : effet de bord avec op�ration � droite d'une affectation
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    int a, b ;
    cout << "entrez la valeur a = " ;
    cin >> a ;
    b = a++ ;
    cout << "b contient a+1, b = " << b ;
    cout << endl << "contenu de a = " << a ;

    return 0;
}
