/* programme etBinaire (exercice8)
*  but : op�ration logique ET entre 2 nombres binaires
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <string>
#include <sstream>
#include <math.h>

using namespace std;

int main()
{
    // d�clarations
    string binaire ;
    int k ;
    unsigned int nb1, nb2 ;
    unsigned int resultat ;

    // saisie du  premier nombre binaire
    cout << "Entrez un premier nombre binaire = " ;
    cin >> binaire ;

    // conversion en entier du premier nombre
    nb1 = 0 ;
    k = 0 ;
    while (binaire.size() > 0)
    {
        // extration du dernier caract�re de binaire
        string s = binaire.substr(binaire.size()-1, 1) ;
        istringstream myStream(s);
        int result ;
        // conversion du caract�re en entier
        myStream>>result ;
        // ajout dans nb du bit r�cup�r� multipli� par la puissance de 2
        nb1 += result * pow(2, k) ;
        // on enl�ve le dernier caract�re
        binaire = binaire.substr(0, binaire.size()-1) ;
        k++ ;
    }
    cout << "conversion en base 10 = " << nb1 ;

    // saisie du  second nombre binaire
    cout << endl << "Entrez un second nombre binaire = " ;
    cin >> binaire ;

    // conversion en entier du second nombre
    nb2 = 0 ;
    k = 0 ;
    while (binaire.size() > 0)
    {
        // extration du dernier caract�re de binaire
        string s = binaire.substr(binaire.size()-1, 1) ;
        istringstream myStream(s);
        int result ;
        // conversion du caract�re en entier
        myStream>>result ;
        // ajout dans nb du bit r�cup�r� multipli� par la puissance de 2
        nb2 += result * pow(2, k) ;
        // on enl�ve le dernier caract�re
        binaire = binaire.substr(0, binaire.size()-1) ;
        k++ ;
    }
    cout << "conversion en base 10 = " << nb2 ;

    // calcul du ET entre les 2
    resultat = nb1 & nb2 ;
    cout << endl << "r�sultat du ET en base 10 = " << resultat ;

    // conversion du r�sultat pour l'affichage
    binaire = "" ;
    while (resultat != 0)
    {
        // r�cup�ration du reste et conversion en caract�re
        char c = (resultat%2) + '0' ;
        // concat�nation du caract�re
        binaire = c + binaire ;
        resultat = resultat / 2 ;
    }
    cout << endl << "conversion en binaire = " << binaire ;

    return 0;
}
