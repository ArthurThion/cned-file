/* programme sommePuissances2 (exercice4)
*  but : calcul de la somme de puissances de 2
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int n, k, somme = 0 ;

    // saisie de la puissance maximale
    cout << "entrez la valeur max = " ;
    cin >> n ;

    // calcul et affichage de la somme des puissances de 2
    for (k=1 ; k<=n ; k++)
    {
        somme = somme + pow(2, k) ;
    }
    cout << "La somme des 2 puissance k = " << somme ;

    return 0;
}
