/* programme premier
*  but : recherche si un nombre saisi est premier ou non
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    // d�clarations et initialisations
    int val, diviseur = 2 ;
    bool premier = true ;

    // saisie du nombre � tester
    cout << "entrez un nombre entier > 1 : " ;
    cin >> val ;

    // boucle sur la recherche d'un diviseur
    while (premier && diviseur*diviseur<=val)
    {
        if (val % diviseur == 0)
        {
            premier = false ;
        }
        diviseur = diviseur + 1 ;
    }

    // affichage du message correspondant au nombre
    if (premier)
    {
        cout << val << " est premier" ;
    }
    else
    {
        cout << val << " n'est pas premier" ;
    }

    return 0;
}
