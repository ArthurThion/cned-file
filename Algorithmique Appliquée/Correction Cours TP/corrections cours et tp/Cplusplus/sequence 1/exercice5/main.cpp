/* programme multPuissances2 (exercice5)
*  but : calcul du produit de puissances de 2
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int n, k, produit = 1 ;

    // saisie de la puissance maximale
    cout << "entrez la valeur max = " ;
    cin >> n ;

    // calcul et affichage du produit des puissances de 2
    for (k=1 ; k<=n ; k++)
    {
        produit = produit * pow(2, k) ;
    }
    cout << "Le produit des 2 puissance k = " << produit ;

    return 0;
}
