/* programme tableDeMultiplication (exercice1)
*  but : afichage d'une table de multiplication
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    int val, k = 1 ;

    // saisie de la valeur
    cout << "Entrez un nombre entier = " ;
    cin >> val ;

    // affichage de la table de multiplication pour cette valeur
    while (k <= 10)
    {
        cout << val << " * " << k << " = " << (val*k) << endl ;
        k = k + 1 ;
    }

    return 0;
}
