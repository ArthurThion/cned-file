/* programme polynome
*  but : saisie d'un polynome et calcul de f(x) en fonction de x
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    // d�clarations
    unsigned int degre ; // degr� du polyn�me
    float a[7] ; // tableau de 7 cases (num�rot�es de 0 � 6)
    char choix ; // choix de l'utilisateur pour continuer ou non
    float x ; // valeur pour le x � remplacer dans le polyn�me
    float resultat ; // r�sultat du calcul

    // saisie du polyn�me
    do {
        cout << "Entrez le degr\x82 du polynome (maximum 6) = " ;
        cin >> degre ;
    }while (degre>6) ;

    // saisie des termes du polynome
    for (int k=0 ; k<=degre ; k++) {
        cout << "a" << k << " = " ;
        cin >> a[k] ;
    }

    // affichage du polyn�me
    cout << endl << "f(X) = " << a[0] ;
    for (int k=1 ; k<=degre ; k++) {
        cout << " + " << a[k] << "*X^" << k ;
    }
    cout << endl ;

    // boucle sur la saisie des valeurs de x et le calcul de f(x)
    do {
        // saisie de x
        cout << "X = " ;
        cin >> x ;
        // calcul de f(x)
        resultat = 0 ;
        for (int k=0 ; k<=degre ; k++) {
            resultat += a[k] * pow(x, k) ;
        }
        cout << "f(X) = " << resultat ;
        // demade si l'utilisateur veut continuer
        cout << endl << "Voulez-vous continuer ? (O/N) = " ;
        cin >> choix ;
    }while (choix == 'O' || choix == 'o') ;


    return 0;
}
