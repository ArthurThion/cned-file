#include <iostream>

using namespace std;

int main()
{
    int a, b, r ;
    cout << "entrez le premier nombre = " ;
    cin >> a ;
    cout << "entrez le second nombre = " ;
    cin >> b ;
    r = a % b ;
    while (r != 0)
    {
        a = b ;
        b = r ;
        r = a % b ;
    }
    cout << "le PGCD est " << b ;
    return 0;
}
