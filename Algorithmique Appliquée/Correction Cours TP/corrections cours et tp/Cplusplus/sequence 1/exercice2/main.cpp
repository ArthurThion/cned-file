/* programme tableDeMultiplication (exercice2)
*  but : afichage d'une table de multiplication avec contr�le de saisie
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    int val, k ;

    // saisie de la valeur avec c�ntrole de saisie
    do
    {
        cout << "Entrez un nombre entier = " ;
        cin >> val ;
    }while (val<1 || val>9) ;

    // affichage de la table de multiplication pour cette valeur
    for (k=1 ; k<=10 ; k++)
    {
        cout << val << " * " << k << " = " << (val*k) << endl ;
    }

    return 0;
}
