/* programme somme (exercice3)
*  but : somme de nombres entre 2 bornes
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    int min, max, k, somme = 0 ;

    // saisie des bornes
    cout << "entrez la valeur min = " ;
    cin >> min ;
    cout << "entrez la valeur max = " ;
    cin >> max ;

    // calcul et affichage de la somme
    for (k=min ; k<=max ; k++)
    {
        somme = somme + k ;
    }
    cout << "La somme des valeurs est = " << somme ;

    return 0;
}
