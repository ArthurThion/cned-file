/* programme conversions (exercice7)
*  but : conversions d'entiers en binaires et vice-versa
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>
#include <string>
#include <sstream>

using namespace std;

int main()
{
    char choix ;
    string binaire ;
    int nb ;

    // boucle sur le menu
    choix = 'Z' ;
    while (choix != 'Q' && choix != 'q')
    {

        // afficher le menu
        cout << endl << "conversion entier vers binaire ........ 1" ;
        cout << endl << "conversion binaire vers entier ........ 2" ;
        cout << endl << "quitter ............................... Q" ;
        cout << endl << "votre choix ........................... " ;
        cin >> choix ;

        // conversion entier vers binaire
        if (choix == '1')
        {
            binaire = "" ;
            cout << "entrer un entier = " ;
            cin >> nb ;
            while (nb != 0)
            {
                // r�cup�ration du reste et conversion en caract�re
                char c = (nb%2) + '0' ;
                // concat�nation du caract�re
                binaire = c + binaire ;
                nb = nb / 2 ;
            }
            cout << "conversion en binaire = " + binaire ;
        }
        else
        {

            // conversion binaire vers entier
            if (choix == '2')
            {
                nb = 0 ;
                int k = 0 ;
                cout << "entrer un nombre binaire = " ;
                cin >> binaire ;
                while (binaire.size() > 0)
                {
                    // extration du dernier caract�re de binaire
                    string s = binaire.substr(binaire.size()-1, 1) ;
                    istringstream myStream(s);
                    int result ;
                    // conversion du caract�re en entier
                    myStream>>result ;
                    // ajout dans nb du bit r�cup�r� multipli� par la puissance de 2
                    nb += result * pow(2, k) ;
                    // on enl�ve le dernier caract�re
                    binaire = binaire.substr(0, binaire.size()-1) ;
                    k++ ;
                }
                cout << "conversion en base 10 = " << nb ;
            }

        }

     }


    return 0;
}
