/* programme matriceInverse (tp 2)
*  but : saisie d'une matrice carr� et inversion de la matrice
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <myconio.h>
#include <string.h>

using namespace std;

#define N 3

int main()
{
    // d�clarations
    float mdep[N][N] ; // matrice de d�part � inverser
    float mtrans[N][N] ; // matrice transitoire pour le calcul
    float minv[N][N] ; // matrice invers�e
    float mid[N][N] = {0} ; // matrice identit�

    // initialisation de la matrice identit� (remplissage des 1 en diagonale)
    for (int i=0 ; i<N ; i++) {
        mid[i][i] = 1 ;
    }

    // affichage de la structure de la matrice � saisir
    for (int i=0 ; i<N ; i++) {
        for (int j=0 ; j<N ; j++) {
            gotoxy(i*10+1, j*2+1) ; cout << "." ;
        }
    }

    // saisie de la matrice NxN
    for (int i=0 ; i<N ; i++) {
        for (int j=0 ; j<N ; j++) {
            gotoxy(i*10+1, j*2+1) ; cin >> mdep[i][j] ;
        }
    }

    //--- calcul de la matrice inverse ---
    memcpy(mtrans, mdep, sizeof mdep); // mtrans <-- mdep
    memcpy(minv, mid, sizeof mid); // minv <-- mid
    // parcours des colonnes
    for (int i=0 ; i<N ; i++) {
        // parcours des lignes de la colonne
        for (int j=0 ; j<N ; j++) {
            // si on trouve un 0 sur la diagonale de la matrice de d�part, inversion impossible
            if (mdep[i][i] == 0) {
                cout << "inversion inmpossible" ;
                return 0 ;
            }
            // calculs
            mtrans[i][j] = mdep[i][j] / mdep[i][i] ;
            mid[i][j] = minv[i][j] / mdep[i][i] ;
        }
        memcpy(mdep, mtrans, sizeof mtrans); // mdep <-- mtrans
        memcpy(minv, mid, sizeof mid); // minv <-- mid
        // parcours complet colonnes/lignes except� la colonne o� on se trouve (i)
        for (int k=0 ; k<N ; k++) {
            if (k != i) {
                for (int j=0 ; j<N ; j++) {
                    // calculs
                    mtrans[k][j] = mdep[k][j] - mdep[i][j] * mdep[k][i] ;
                    mid[k][j] = minv[k][j] - minv[i][j] * mdep[k][i] ;
                }
            }
        }
        memcpy(mdep, mtrans, sizeof mtrans); // mdep <-- mtrans
        memcpy(minv, mid, sizeof mid); // minv <-- mid
    }

    // affichage de la matrice inverse
    for (int i=0 ; i<N ; i++) {
        for (int j=0 ; j<N ; j++) {
            gotoxy(i*10+1, j*2+10) ; cout << minv[i][j] ;
        }
    }

    return 0;
}
