/* programme cryptage (tp 1)
*  but : permet de crypter un message et de le d�crypter
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
    // d�clarations
    string message, resultat ; // message d'origine et message crypt�
    unsigned int milieu ; // position du milieu du message
    string cle = "cryptographie" ; // cl� utilis�e pour le cryptage
    char decalage = '-' ; // valeur de d�calage

    // saisie du message
    cout << "Entrez un message = " ;
    cin >> message ;

    //--- cryptage du message ---
    // inversion entre le d�but et la fin (le message est coup� en 2)
    milieu = message.size() / 2 ;
    message = message.substr(milieu, message.size()-milieu) + message.substr(0, milieu) ;
    // oux entre chaque lettre du message et une des lettres de la cl�
    resultat = "" ;
    for (unsigned int k=0 ; k<message.size() ; k++) {
        resultat += (message[k] ^ cle[k%cle.size()]) + decalage ;
    }

    // affichage du message crypt�
    cout << "crypt\x82 = " << resultat ;

    //--- op�rations inverses (d�cryptage) ---
    // � nouveau oux permet de revenir � la valeur d'origine
    message = "" ;
    for (unsigned int k=0 ; k<resultat.size() ; k++) {
        message += ((resultat[k] - decalage) ^ cle[k%cle.size()])  ;
    }
    // attention le milieu est d�cal� si la longueur du message est impaire
    if (message.size()%2!=0) {
        milieu++ ;
    }
    message = message.substr(milieu, message.size()-milieu) + message.substr(0, milieu) ;


    // affichage du message d�crypt�
    cout << endl << "message d'origine = " << message ;

    return 0;
}
