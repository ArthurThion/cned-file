/* programme facteursPremiers (exercice 6)
*  but : saisir des valeurs, afficher les facteurs premiers, puis au final
*        afficher une seule fois chaque facteur premier qui est apparu
*        Ceci est la version non optimis�e
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    // d�clarations
    unsigned int m[100][50] = {0} ;
    unsigned int i, j, x, y ; // indices de parcours de la matrice
    unsigned int valeur, diviseur ; // valeur � diviser et diviseur
    unsigned int maxL = 0, maxC ; // nbre de lignes et de colonnes remplies dans m
    bool present ; // m�morisation si le facteur a d�j� �t� affich�

    // boucle sur la saisie des valeurs
    cout << "entrer une valeur (0 pour finir) = " ;
    cin >> valeur ;
    while (valeur != 0) {
        // recherche des facteurs premiers
        maxC = 0 ;
        diviseur = 2 ;
        while (diviseur<=valeur) {
            // si on trouve un facteur premier, on le m�morise
            if (valeur%diviseur == 0) {
                m[maxL][maxC++] = diviseur ;
                valeur /= diviseur ;
            }else{
                diviseur++ ;
            }
        }
        // afichage des facteurs premiers de la valeur saisie
        cout << "facteurs premiers = " ;
        j = 0 ;
        while (m[maxL][j] != 0) {
            cout << m[maxL][j++] << " " ;
        }
        // saisie d'une nouvelle valeur
        maxL++ ;
        cout << endl << "entrer une valeur (0 pour finir) = " ;
        cin >> valeur ;
    }

    // affichage une seule fois de chaque facteur premier
    cout << endl << "Tous les facteurs premiers = " ;
    for (i=0 ; i<maxL ; i++) {
        j = 0 ;
        // m �tant rempli de 0, on s'arr�te d�s qu'on tombe en colonne sur un 0
        while (m[i][j] != 0) {
            // on cherche si dans les lignes pr�c�dentes il �tait d�j� pr�sent
            present = false ;
            x = 0 ;
            while (x<i && !present) {
                y = 0 ;
                while (m[x][y]!=0 && !present) {
                    if (m[x][y]==m[i][j]) {
                        present = true ;
                    }
                    y++ ;
                }
                x++ ;
            }
            // on cherche aussi si au d�but de la ligne il n'�tait pas pr�sent
            y = 0 ;
            while (y<j && !present) {
                if (m[i][y]==m[i][j]) {
                    present = true ;
                }
                y++ ;
            }
            // s'il n'�tait pr�sent nulle part, on l'affiche
            if (!present) {
                cout << m[i][j] << " " ;
            }
            // passage � la case suivante
            j++ ;
        }
    }

    return 0;
}
