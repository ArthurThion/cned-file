/* programme 50premiers (exercice 2)
*  but : affichage des 50 premiers nombres premiers avec recherche optimis�e
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    // d�clarations
    unsigned int premier[50] ; // tableau qui m�morisera les nombres premiers
    unsigned int k ; // k sera l'indice de boucle
    unsigned int max = 1 ; // max contiendra le nombre de cases remplies dans premier
    unsigned int nb = 3 ; // variable qui sera � tester

    // initialisation de la premi�re case
    premier[0] = 2 ;

    // boucle sur la recherche des premiers
    while (max<50) {
        // test si le nombre contenu dans nb est premier
        k = 0 ;
        while (k<max && pow(premier[k], 2)<nb && nb%premier[k]!=0) {
            k++ ;
        }
        // si nb est premier, il est m�moris�
        if (nb%premier[k]!=0) {
            premier[max++] = nb ;
        }
        // passage au nombre suivant � tester
        nb++ ;
    }

    // affichage des 50 nombres premiers
    for (k=0 ; k<50 ; k++) {
        cout << premier[k] << " " ;
    }

    return 0;
}
