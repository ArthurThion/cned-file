/* programme somme2matrices (exercice 3)
*  but : saisie de 2 matrices et
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <myconio.h>

using namespace std;

int main()
{

    // d�clarations
    unsigned int m1[4][4], m2[4][4] ;

    // affichage de la structure des matrices � saisir
    for (int i=0 ; i<4 ; i++) {
        for (int j=0 ; j<4 ; j++) {
            gotoxy(i*4+1, j*2+1) ; cout << "." ;
            gotoxy(i*4+21, j*2+1) ; cout << "." ;
        }
    }
    gotoxy(16, 3) ; cout << "+" ;
    gotoxy(36, 3) ; cout << "=" ;

    // saisie d'une premi�re matrice 4x4
    for (int i=0 ; i<4 ; i++) {
        for (int j=0 ; j<4 ; j++) {
            gotoxy(i*4+1, j*2+1) ; cin >> m1[i][j] ;
        }
    }

    // saisie d'une seconde matrice 4x4
    for (int i=0 ; i<4 ; i++) {
        for (int j=0 ; j<4 ; j++) {
            gotoxy(i*4+21, j*2+1) ; cin >> m2[i][j] ;
        }
    }

    // calcul et affichage de la matrice somme
    for (int i=0 ; i<4 ; i++) {
        for (int j=0 ; j<4 ; j++) {
            gotoxy(i*4+41, j*2+1) ; cout << (m1[i][j]+m2[i][j]) ;
        }
    }

    return 0;
}
