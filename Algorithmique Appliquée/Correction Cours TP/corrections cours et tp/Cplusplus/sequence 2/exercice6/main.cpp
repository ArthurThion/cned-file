/* programme facteursPremiers (exercice 7)
*  but : saisir des valeurs, afficher les facteurs premiers, puis au final
*        afficher une seule fois chaque facteur premier qui est apparu
*        Ceci est la version optimis�e
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>

using namespace std;

int main()
{
    // d�clarations
    unsigned int facteur[100] ;
    unsigned int k ; // indice de parcours du vecteur
    unsigned int valeur, diviseur ; // valeur � diviser et diviseur
    unsigned int max = 0 ; // nbre de cases remplies dans facteur

    // boucle sur la saisie des valeurs
    cout << "entrer une valeur (0 pour finir) = " ;
    cin >> valeur ;
    while (valeur != 0) {
        // recherche des facteurs premiers
        diviseur = 2 ;
        while (diviseur<=valeur) {
            // si on trouve un facteur premier
            if (valeur%diviseur == 0) {
                // on l'affiche
                cout << diviseur << " " ;
                // on regarde s'il a d�j� �t� m�moris�
                k = 0 ;
                while (k<max && facteur[k]!=diviseur) {
                    k++ ;
                }
                if (k==max) {
                    facteur[max++] = diviseur ;
                }
                // on divise la valeur pour �viter les facteurs non premiers
                valeur /= diviseur ;
            }else{
                diviseur++ ;
            }
        }

        // saisie d'une nouvelle valeur
        cout << endl << "entrer une valeur (0 pour finir) = " ;
        cin >> valeur ;
    }

    // affichage une seule fois de chaque facteur premier
    cout << endl << "Tous les facteurs premiers = " ;
    for (k=0 ; k<max ; k++) {
        cout << facteur[k] << " " ;
    }

    return 0;
}
