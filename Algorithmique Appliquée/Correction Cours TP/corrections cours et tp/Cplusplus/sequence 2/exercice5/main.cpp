/* programme  (exercice 5)
*  but :
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <vector>

using namespace std;

class forme {
    public :
        forme() {} ;
        virtual ~forme() {} ;
        virtual float surface = 0 ;
        virtual float perimetre = 0 ;
} ;

class carre : public forme {
    private :
        float cote ;
    public :
        carre(float unCote) {cote = unCote ;}
        float surface() {return cote*cote ;}
        float perimetre() {return cote*4 ;}
        virtual ~carre() {} ;
} ;

class rectangle : public forme {
    private :
        float longueur, largeur ;
    public :
        rectangle(float l, float L) {longueur = L ; largeur = l ;}
        float surface() {return longueur*largeur ;}
        float perimetre() {return 2*longueur + 2*largeur ;}
        virtual ~rectangle() {} ;
} ;



int main()
{
    vector <forme*> tab ;

    tab.push_back(new carre(3)) ;
    tab.push_back(new rectangle(2, 3)) ;

    cout << endl << "surface du carr� = " << tab[0]->surface() ;
    cout << endl << "surface du rectangle = " << tab[1]->surface() ;

    for (int k=0 ; k<tab.size() ; k++) {
        delete tab[k] ;
        tab[k] = 0 ;
    }

    return 0;
}
