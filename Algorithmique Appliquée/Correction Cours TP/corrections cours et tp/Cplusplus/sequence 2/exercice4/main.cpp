/* programme villes (exercice 4)
*  but : permet d'afficher des distances entre villes
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <string>
#include <myconio.h>

using namespace std;

int main()
{
    // d�clarations
    string ville[10] = {"Paris", "Lyon", "Marseille", "Bordeaux", "Nice"} ;
    unsigned int dist[5][5] = {0, 400, 800, 700, 1000, 0, 0, 400, 500, 600, 0, 0, 0, 650, 200, 0, 0, 0, 0, 850, 0, 0, 0, 0, 0} ;

    // affichage des villes en t�tes de colonnes
    for (int j=0 ; j<5 ; j++) {
        gotoxy((j+1)*12, 1) ; cout << ville[j] ;
    }
    // affichage de la matrice triangulaire des distances
    for (int i=0 ; i<5 ; i++) {
        // affichage de la ville en t�te de ligne
        gotoxy(1, i*2+3) ; cout << ville[i] ;
        // boucle sur les distances par rapport � cette ville
        for (int j=0 ; j<i ; j++) {
            gotoxy((j+1)*12, i*2+3) ; cout << dist[j][i] ;
        }
    }

    return 0;
}
