/* programme 50premiers (exercice 6)
*  but : reprise de l'exercice 2 de la s�quence 2, en incluaant la biblioth�que maBiblio
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>
#include <maBiblio.h>

using namespace std;

int main()
{
    // d�clarations
    unsigned int p[50] ; // tableau qui m�morisera les nombres premiers
    unsigned int k ; // k sera l'indice de boucle
    unsigned int max = 1 ; // max contiendra le nombre de cases remplies dans premier
    unsigned int nb = 3 ; // variable qui sera � tester

    // initialisation de la premi�re case
    p[0] = 2 ;

    // boucle sur la recherche des premiers
    while (max<50) {

        // si nb est premier, il est m�moris�
        if (premier(nb)) {
            p[max++] = nb ;
        }
        // passage au nombre suivant � tester
        nb++ ;
    }

    // affichage des 50 nombres premiers
    for (k=0 ; k<50 ; k++) {
        cout << p[k] << " " ;
    }

    return 0;
}
