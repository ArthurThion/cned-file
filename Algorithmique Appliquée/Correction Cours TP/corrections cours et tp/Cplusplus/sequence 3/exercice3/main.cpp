/* programme etBinaire (exercice 3)
*  but : reprise de l'exercice 8 de la s�quence 1, mais cette fois avec des fonctions
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <string>
#include <sstream>
#include <math.h>

using namespace std;

/* fonction convertBinaireEntier
*  but : convertit un binaire en entier
*  entr�e : unBinaire (cha�ne) re�oit le binaire � convertir
*  sortie : entier contenant la conversion
*/
unsigned int convertBinaireEntier(string unBinaire) {
    unsigned int nb = 0 ;
    unsigned int k = 0 ;
    while (unBinaire.size() > 0)
    {
        // extration du dernier caract�re de binaire
        string s = unBinaire.substr(unBinaire.size()-1, 1) ;
        istringstream myStream(s);
        int result ;
        // conversion du caract�re en entier
        myStream>>result ;
        // ajout dans nb du bit r�cup�r� multipli� par la puissance de 2
        nb += result * pow(2, k) ;
        // on enl�ve le dernier caract�re
        unBinaire = unBinaire.substr(0, unBinaire.size()-1) ;
        k++ ;
    }
    return nb ;
}

/* fonction convertEntierBinaire
*  but : convertit une nombre entier en binaire
*  entr�e : unEntier (entier) re�oit le nombre � convertir
*  sortie : cha�ne contenant la conversion
*/
string convertEntierBinaire( unsigned int unEntier) {
    string binaire = "" ;
    while (unEntier != 0)
    {
        // r�cup�ration du reste et conversion en caract�re
        char c = (unEntier%2) + '0' ;
        // concat�nation du caract�re
        binaire = c + binaire ;
        unEntier = unEntier / 2 ;
    }
    return binaire ;
}


using namespace std;

int main()
{
    // d�clarations
    string binaire ;
    unsigned int nb1, nb2 ;
    unsigned int resultat ;

    // saisie du  premier nombre binaire
    cout << "Entrez un premier nombre binaire = " ;
    cin >> binaire ;

    // conversion en entier du premier nombre
    nb1 = convertBinaireEntier(binaire) ;
    cout << "conversion en base 10 = " << nb1 ;

    // saisie du  second nombre binaire
    cout << endl << "Entrez un second nombre binaire = " ;
    cin >> binaire ;

    // conversion en entier du second nombre
    nb2 = convertBinaireEntier(binaire) ;
    cout << "conversion en base 10 = " << nb2 ;

    // calcul du ET entre les 2
    resultat = nb1 & nb2 ;
    cout << endl << "r�sultat du ET en base 10 = " << resultat ;

    // conversion du r�sultat pour l'affichage
    cout << endl << "conversion en binaire = " << convertEntierBinaire(resultat) ;

    return 0;
}
