/* programme 50premiers (exercice 4)
*  but : reprise de l'exercice 6 de la s�quence 1 avec une fonction
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>
#include <maBiblio.h>

using namespace std;

int main()
{
    // d�clarations et initialisations
    unsigned int val ;

    // saisie du premier nombre � tester
    cout << "entrez un nombre entier (0 pour finir) : " ;
    cin >> val ;

    // boucle g�n�rale sur les tests
    while (val != 0) {

        // affichage du message correspondant au nombre
        if (premier(val)) {
            cout << val << " est premier" ;
        }else{
            cout << val << " n'est pas premier" ;
        }

        // saisie d'un nombre � tester
        cout << endl << "entrez un nombre entier (0 pour finir) : " ;
        cin >> val ;
    }

    return 0;
}
