/* programme nombres (exercice7)
*  but : différents traitements sur des nombres
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <maBiblio.h>

using namespace std;

int main()
{
    char choix ;
    unsigned int nb ;

    // boucle sur le menu
    choix = 'Z' ;
    while (choix != 'Q' && choix != 'q') {

        // afficher le menu
        cout << endl << "50 premiers multiples d'un nombre ........ 1" ;
        cout << endl << "facteurs premiers d'un nombre ............ 2" ;
        cout << endl << "50 premiers nombres premiers ............. 3" ;
        cout << endl << "quitter .................................. Q" ;
        cout << endl << "votre choix .............................. " ;
        cin >> choix ;

        // 50 premiers multiples d'un nombre
        if (choix == '1') {
            cout << "Entrez un nombre = " ;
            cin >> nb ;
            multiples(nb, 50) ;
        }else{
            // facteurs premiers d'un nombre
            if (choix == '2') {
                cout << "Entrez un nombre = " ;
                cin >> nb ;
                facteursPremiers(nb) ;
            }else{
                // 50 premiers nombres premiers
                if (choix == '3') {
                    nombresPremiers(50) ;
                }
            }
        }

    }

    return 0;
}
