/* programme racine (exercice 2)
*  but : permet de saisie plusieurs valeurs et d'afficher � chaque fois la racine carr�e
*  auteur : Emds
*  date : 12/07/2011
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    // d�clarations
    unsigned int nb ;

    // boucle sur la saisie des valeurs
    cout << "Entrez une valeur (0 pour finir) = " ;
    cin >> nb ;
    while (nb != 0) {
        // affichage de la racine carr�e
        cout << "racine carr\x82e = " << sqrt(nb) ;
        // saisie d'une nouvelle valeur
        cout << endl << "Entrez une valeur = " ;
        cin >> nb ;
    }

    return 0;
}
